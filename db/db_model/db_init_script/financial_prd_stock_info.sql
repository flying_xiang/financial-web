CREATE DATABASE  IF NOT EXISTS `financial_prd` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `financial_prd`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: financial_prd
-- ------------------------------------------------------
-- Server version	5.6.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `stock_info`
--

DROP TABLE IF EXISTS `stock_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `cur_price` double DEFAULT NULL,
  `yes_price` double DEFAULT NULL,
  `tod_open_price` double DEFAULT NULL,
  `count` double DEFAULT NULL,
  `out_count` double DEFAULT NULL,
  `inner_count` double DEFAULT NULL,
  `buy_one_price` double DEFAULT NULL,
  `buy_one_count` double DEFAULT NULL,
  `buy_two_price` double DEFAULT NULL,
  `buy_two_count` double DEFAULT NULL,
  `buy_three_price` double DEFAULT NULL,
  `buy_three_count` double DEFAULT NULL,
  `buy_four_price` double DEFAULT NULL,
  `buy_four_count` double DEFAULT NULL,
  `buy_five_price` double DEFAULT NULL,
  `buy_five_count` double DEFAULT NULL,
  `sell_one_price` double DEFAULT NULL,
  `sell_one_count` double DEFAULT NULL,
  `sell_two_price` double DEFAULT NULL,
  `sell_two_count` double DEFAULT NULL,
  `sell_three_price` double DEFAULT NULL,
  `sell_three_count` double DEFAULT NULL,
  `sell_four_price` double DEFAULT NULL,
  `sell_four_count` double DEFAULT NULL,
  `sell_five_price` double DEFAULT NULL,
  `sell_five_count` double DEFAULT NULL,
  `last_deal` varchar(512) DEFAULT NULL,
  `data_time` datetime DEFAULT NULL,
  `fluctuate` double DEFAULT NULL,
  `fluctuate_percent` double DEFAULT NULL,
  `top_price` double DEFAULT NULL,
  `low_price` double DEFAULT NULL,
  `features` varchar(256) DEFAULT NULL,
  `total_count` double DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `exchange_rate` double DEFAULT NULL,
  `per` double DEFAULT NULL,
  `top_price2` double DEFAULT NULL,
  `low_price2` double DEFAULT NULL,
  `amplitude` double DEFAULT NULL,
  `flow_values` double DEFAULT NULL,
  `total_values` double DEFAULT NULL,
  `pb` double DEFAULT NULL,
  `max_price` double DEFAULT NULL,
  `min_price` double DEFAULT NULL,
  `date` date DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Reference_1` (`code`),
  CONSTRAINT `FK_Reference_1` FOREIGN KEY (`code`) REFERENCES `stock` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=1367474 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-18 10:38:46
