package com.ltw.dreamer.common.schedule;

import java.util.List;

import javax.annotation.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import com.ltw.dreamer.stock.model.Stock;
import com.ltw.dreamer.stock.service.StockBackService;
import com.ltw.dreamer.stock.service.StockInfoService;
import com.ltw.dreamer.stock.service.StockService;

@Component
public class DailyUpdateTask {
	@Resource
	private StockService stockService = null;

	@Resource
	private StockInfoService stockInfoService = null;
	
	@Resource
	private StockBackService stockBackService = null;
	
	@Scheduled(cron="0 0 16 * * MON-FRI")
	public void updateStockDailyInfo() {
		List<Stock> stocks = stockService.selectAllStock();
		if (stocks == null) return ;

		for (Stock stock : stocks) {
			stockInfoService.updateStockDailyInfo(stock);
			stockService.updateStockBasicInfo(stock);
			stockBackService.updateStockBackupInfo(stock);
		}
	}
}
