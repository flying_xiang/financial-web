package com.ltw.dreamer.common.schedule;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ltw.dreamer.report.service.BalanceSheetService;
import com.ltw.dreamer.report.service.CashFlowService;
import com.ltw.dreamer.report.service.ProfitStatementService;
import com.ltw.dreamer.stock.model.Stock;
import com.ltw.dreamer.stock.service.StockService;

@Component
public class FinancialReportTask {
	@Resource
	private StockService stockService = null;
	
	@Resource
	private BalanceSheetService balanceSheetService = null;
	
	@Resource
	private CashFlowService cashFlowService = null;
	
	@Resource
	private ProfitStatementService profitStatementService = null;
	
	@Scheduled(cron="0 0 20 30 1/1 ?")
	public void updateFinancialReport() throws InterruptedException {
		List<Stock> stocks = stockService.selectAllStock();
		
		for (Stock stock : stocks) {
			balanceSheetService.updateBalanceSheet(stock);
			cashFlowService.updateCashFlow(stock);
			profitStatementService.updateProfitStatement(stock);
		}
	}
}
