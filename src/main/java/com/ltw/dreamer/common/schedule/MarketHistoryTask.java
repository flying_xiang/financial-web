package com.ltw.dreamer.common.schedule;

import java.util.List;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ltw.dreamer.stock.model.Stock;
import com.ltw.dreamer.stock.service.MarketHistoryService;
import com.ltw.dreamer.stock.service.StockService;
@Component
public class MarketHistoryTask {
	public static final Logger LOGGER = Logger.getLogger(MarketHistoryTask.class);

	@Resource
	private StockService stockService = null;
	
	@Resource
	private MarketHistoryService marketHistoryService = null;
	
	@Scheduled(cron="0 0 0 ? * SAT")
	public void updateMarketHistory() {
		List<Stock> stocks = stockService.selectAllStock();
		for (Stock stock : stocks)
			marketHistoryService.updateMarketHistory(stock);
	}
	
	public void fixMarketHistoryData() {
		List<Stock> stocks = stockService.selectAllStock();
		for (Stock stock : stocks) {
			marketHistoryService.fixMarketHistoryData(stock);
		}
	}
}
