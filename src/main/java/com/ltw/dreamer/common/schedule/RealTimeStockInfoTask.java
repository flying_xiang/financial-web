package com.ltw.dreamer.common.schedule;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ltw.dreamer.sms.service.RealTimeStockInfoService;
import com.ltw.dreamer.stock.model.Stock;
import com.ltw.dreamer.stock.service.StockInfoService;
import com.ltw.dreamer.stock.service.StockService;

@Component
public class RealTimeStockInfoTask {
	public static final Logger LOGGER = Logger.getLogger(RealTimeStockInfoTask.class);
	
	@Resource
	private StockInfoService stockInfoService = null;
	
	@Resource
	private StockService stockService = null;

	@Resource
	RealTimeStockInfoService realTimeStockInfoService = null;
	
	@Scheduled(cron="0 0/20 9-11,13-14 ? * MON-FRI")
	public void notifyRealTimeStockInfo() {
		LOGGER.info("准备处理股票实时交易信息并推送至指定地址....");
		List<Stock> stocks = stockService.selectAllStock();
		if (stocks == null) return ;

		for (Stock stock : stocks) {
			stockInfoService.updateStockDailyInfo(stock);
			stockService.updateStockBasicInfo(stock);
		}
		
		realTimeStockInfoService.notifyRealTimeStockInfo();
		LOGGER.info("完成股票实时交易信息处理.");
	}
}
