package com.ltw.dreamer.common.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.springframework.format.datetime.DateFormatter;

public class DateUtil {
	public static final int getCurrentYear() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.YEAR);
	}
	
	public static final int getCurrentSeason() {
		Calendar calendar = Calendar.getInstance();
		int month = calendar.get(Calendar.MONTH);
		return month/3 + 1;
	}
	
	public static final String getDailyDateString(Date date) {
		if (date == null) return "";
		
		DateFormatter formatter = new DateFormatter("YYYY-MM-dd");
		return formatter.print(date, Locale.CHINA);
	}
	
	public static final String getCurrentDateString() {
		DateFormatter formatter = new DateFormatter("YYYY-MM-dd HH:mm:ss");
		return formatter.print(Calendar.getInstance().getTime(), Locale.CHINA);
	}
	
	public static final int getSequence(Date date) {  
        Calendar cal = Calendar.getInstance();    
        cal.set(2015, Calendar.SEPTEMBER, 1);
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(date);    
        long time2 = cal.getTimeInMillis();         
        long between_days=(time2-time1)/(1000*3600*24);  
            
       return Integer.parseInt(String.valueOf(between_days));   
	}
	
	public static double getMultiplyFactor(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 15);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		
		if (date.after(calendar.getTime())) return 1;
		
		calendar.set(Calendar.HOUR_OF_DAY, 13);
		calendar.set(Calendar.MINUTE, 0);
		if (date.after(calendar.getTime()))
			return 1 / (((double)date.getTime() - calendar.getTime().getTime()) / (4 * 60 * 60 * 1000) + 0.5);

		calendar.set(Calendar.HOUR_OF_DAY, 11);
		calendar.set(Calendar.MINUTE, 30);
		if (date.after(calendar.getTime()))
			return 2.00;
		
		calendar.set(Calendar.HOUR_OF_DAY, 9);
		calendar.set(Calendar.MINUTE, 30);
		if (date.after(calendar.getTime()))
			return (4 * 60 * 60 * 1000)/((double)date.getTime() - calendar.getTime().getTime());
		
		return 0;
	}
	
	public static void main(String[] args) {
		// System.out.println(DateUtil.getCurrentSeason());
		// System.out.println(DateUtil.getDailyDateString(Calendar.getInstance().getTime()));
		// System.out.println(DateUtil.getCurrentDateString());
		// System.out.println(DateUtil.getSequence(Calendar.getInstance().getTime()));
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 9);
		calendar.set(Calendar.MINUTE, 31);
		calendar.set(Calendar.SECOND, 0);
		
		System.out.println(DateUtil.getMultiplyFactor(calendar.getTime()));
	}
}
