package com.ltw.dreamer.common.util;

import com.ltw.dreamer.engine.pojo.Line;
import com.ltw.dreamer.engine.pojo.Point;

public class GraphicUtil {
	public static Line createLine(Point point1, Point point2) {
		Line result = new Line();
		result.setK((point1.getY() - point2.getY())/(point1.getX() - point2.getX()));
		result.setB(point1.getY() - result.getK() * point1.getX());
		return result;
	}
	public static boolean pointIsBelowLine(Point point, Line line) {
		return point.getY() - (line.getK() * point.getX() + line.getB()) <= 0;
	}
	public static boolean pointIsUpperLine(Point point, Line line) {
		return point.getY() - (line.getK() * point.getX() + line.getB()) >= 0;
	}
	public static boolean pointIsBelowLine(Point point, Line line, double delta) {
		Line newLine = new Line();
		newLine.setK(line.getK() * (1 + delta));
		newLine.setB(line.getB() * (1 + delta));
		return pointIsBelowLine(point, newLine);
	}
	public static boolean pointIsUpperLine(Point point, Line line, double delta) {
		Line newLine = new Line();
		newLine.setK(line.getK() * (1 + delta));
		newLine.setB(line.getB() * (1 + delta));
		return pointIsUpperLine(point, newLine);
	}
	public static Line createLine(Line line, double delta) {
		Line newLine = new Line();
		newLine.setK(line.getK() * (1 + delta));
		newLine.setB(line.getB() * (1 + delta));
		return newLine;
	}
}
