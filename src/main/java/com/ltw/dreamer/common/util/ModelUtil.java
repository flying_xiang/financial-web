package com.ltw.dreamer.common.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ModelUtil {
	public static void load(Object source, Object target) {
		Field[] fields = target.getClass().getDeclaredFields();
		for (Field targetField : fields) {
			try {
				String fieldName = targetField.getName();
				Field sourceField = source.getClass().getDeclaredField(fieldName);
				if (sourceField == null) continue;
				Method readMethod = new PropertyDescriptor(fieldName, source.getClass()).getReadMethod();
				Method writeMethod = new PropertyDescriptor(fieldName, target.getClass()).getWriteMethod();
				writeMethod.invoke(target, readMethod.invoke(source));
			} catch (Exception e) {
				continue;
			}
		}
	}
}
