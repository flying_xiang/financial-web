package com.ltw.dreamer.common.util;

public class NumbericUtil {
	/**
	 * 计算double的除法，保留两位有效数字
	 * @param value1
	 * @param value2
	 * @return
	 */
	public static Double calDoublDivide(Double value1, Double value2) {
		if (value1 == null || value2 == null)
			return null;
		
		if (value2.doubleValue() == 0)
			return null;
		
		int result = (int)((value1.doubleValue() / value2.doubleValue()) * 100);
		return ((double)result) / 100;
	}
	
	public static final double getMinValue(Double[] array) {
		double minValue = formatNumber(Double.MAX_VALUE);
		for (Double curValue : array) 
			minValue = curValue < minValue ? curValue : minValue;
		return minValue;
	}
	
	public static final double getMaxValue(Double[] array) {
		double maxValue = formatNumber(Double.MIN_VALUE);
		for (Double curValue : array)
			maxValue = curValue > maxValue ? curValue : maxValue;
			
		return maxValue;
	}
	public static final Double createRandomDouble() {
		return formatNumber(Math.random() * 100);
	}

	public static final Double[] createRandomDoubleArray() {
		int size = (int) (Math.random() * 100);
		Double[] doubleArray = new Double[size];
		
		for (int i=0; i<size; i++)
			doubleArray[i] = formatNumber(Math.random() * 100);
		
		return doubleArray;
	}
	
	public static final Double formatNumber(Double dvalue) {
		return ((int)(dvalue*10000) * 1.0) / 10000;
	}
}
