package com.ltw.dreamer.common.util;

import java.util.HashMap;
import java.util.Map;

public class ServiceRegister {
	private static Map<String, Object> servicePool = new HashMap<String, Object>();
	
	public static void register(String id, Object serviceObject) {
		servicePool.put(id, serviceObject);
	}
	
	public static Object unregister(String id) {
		return servicePool.remove(id);
	}
	
	public static Object getService(String id) {
		return servicePool.get(id);
	}
}
