package com.ltw.dreamer.doc.pdf;

import java.io.IOException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

public class PDF {
	/**
	 * 
	 * Main method.
	 * 
	 * @param args
	 *            no arguments needed
	 * 
	 * @throws DocumentException
	 * 
	 * @throws IOException
	 * 
	 */

	public static void main(String[] args) throws DocumentException, IOException {
		String filename = "doc/2015年3季度上市公司行业分类结果.pdf";// pdf文件路径
		inspect(filename); // 调用读取方法
	}

	public static void inspect(String filename)	throws IOException {
		PdfReader reader = new PdfReader(PDF.class.getResource("/").getPath() + filename);
		int num = reader.getNumberOfPages();

		String content = "";

		for (int i = 1; i < num; i++) {
			content += PdfTextExtractor.getTextFromPage(reader, i) + "\r\n";
		}
		System.out.println(content);
	}

}