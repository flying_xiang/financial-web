package com.ltw.dreamer.engine.core;

import java.util.List;

import com.ltw.dreamer.stock.model.StockInfo;

public interface StockWatchDog {
	public boolean shouldAnalyse(List<StockInfo> stockInfoList);
}
