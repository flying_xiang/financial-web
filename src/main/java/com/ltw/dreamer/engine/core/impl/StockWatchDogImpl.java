package com.ltw.dreamer.engine.core.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.ltw.dreamer.engine.core.StockWatchDog;
import com.ltw.dreamer.engine.pojo.StockFilterEnum;
import com.ltw.dreamer.stock.model.StockInfo;

@Service
public class StockWatchDogImpl implements StockWatchDog {
	public static final Logger LOGGER = Logger.getLogger(StockWatchDogImpl.class);
	
	private List<StockFilterEnum> filters = null;
	
	public StockWatchDogImpl() {
	    // 默认需要开启的过滤器
		filters = new ArrayList<StockFilterEnum>();
		filters.add(StockFilterEnum.DATA_NOT_ENOUGH);
		filters.add(StockFilterEnum.PER_IS_TOO_HIGH);
		filters.add(StockFilterEnum.PER_IS_TOO_LOW);
		// filters.add(StockFilterEnum.PRICE_IS_TOO_HIGH);
		// filters.add(StockFilterEnum.FLOW_VALUE_IS_TOO_BIG);
		filters.add(StockFilterEnum.NO_BOUNDARY_STOCK);
		filters.add(StockFilterEnum.NOT_A_MARKET_STOCK);
	}
	public void setFilters(List<StockFilterEnum> filters) {
		this.filters = filters;
	}
	
	public boolean shouldAnalyse(List<StockInfo> stockInfoList) {
		if (filters == null) return true;
		for (StockFilterEnum filter : filters)
			if (filter.doFilter(stockInfoList)) return false;
		
		return true;
	}
}
