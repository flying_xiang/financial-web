package com.ltw.dreamer.engine.filter;

import java.util.List;

import com.ltw.dreamer.stock.model.StockInfo;

public interface StockFilter {
	boolean doFilter(List<StockInfo> stockInfoList);
}
