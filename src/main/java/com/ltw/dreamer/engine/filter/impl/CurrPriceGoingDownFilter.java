package com.ltw.dreamer.engine.filter.impl;

import java.util.List;

import com.ltw.dreamer.engine.filter.StockFilter;
import com.ltw.dreamer.stock.model.StockInfo;

public class CurrPriceGoingDownFilter implements StockFilter {

	public boolean doFilter(List<StockInfo> stockInfoList) {
		if (stockInfoList.size() < 2) return false;
		return stockInfoList.get(0).getCurPrice() < stockInfoList.get(1).getCurPrice();
	}
}
