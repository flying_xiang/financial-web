package com.ltw.dreamer.engine.filter.impl;

import java.util.List;

import com.ltw.dreamer.engine.filter.StockFilter;
import com.ltw.dreamer.stock.model.StockInfo;

public class CurrPriceMaxFilter implements StockFilter {
	public boolean doFilter(List<StockInfo> stockInfoList) {
		StockInfo latestStockInfo = stockInfoList.get(0);
		return latestStockInfo.getCurPrice().doubleValue() == latestStockInfo.getMaxPrice().doubleValue();
	}
}
