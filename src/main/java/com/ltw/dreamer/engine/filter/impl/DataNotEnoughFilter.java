package com.ltw.dreamer.engine.filter.impl;

import java.util.List;

import com.ltw.dreamer.engine.filter.StockFilter;
import com.ltw.dreamer.stock.model.StockInfo;

public class DataNotEnoughFilter implements StockFilter {
	public boolean doFilter(List<StockInfo> stockInfoList) {
		if (stockInfoList == null) return true;
		if (stockInfoList.size() < 40) return true;
		
		return false;
	}
}
