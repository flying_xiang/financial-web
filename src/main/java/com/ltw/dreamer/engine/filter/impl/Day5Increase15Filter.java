package com.ltw.dreamer.engine.filter.impl;

import java.util.List;

import com.ltw.dreamer.engine.filter.StockFilter;
import com.ltw.dreamer.stock.model.StockInfo;

public class Day5Increase15Filter implements StockFilter {
	public boolean doFilter(List<StockInfo> stockInfoList) {
		double curDayPrice = stockInfoList.get(0).getCurPrice();
		double day5Price = stockInfoList.get(4).getCurPrice();
		
		return curDayPrice > day5Price * 1.15;
	}
}
