package com.ltw.dreamer.engine.filter.impl;

import java.util.List;

import com.ltw.dreamer.engine.filter.StockFilter;
import com.ltw.dreamer.stock.model.StockInfo;

public class NoBoundaryStockFilter implements StockFilter {

	public boolean doFilter(List<StockInfo> stockInfoList) {
		for (StockInfo stockInfo : stockInfoList)
			if (stockInfo.getFluctuatePercent() < -11.00 || stockInfo.getFluctuatePercent() > 11.00) return true;
		return false;
	}

}
