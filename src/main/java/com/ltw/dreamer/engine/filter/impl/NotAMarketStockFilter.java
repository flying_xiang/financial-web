package com.ltw.dreamer.engine.filter.impl;

import java.util.List;

import com.ltw.dreamer.engine.filter.StockFilter;
import com.ltw.dreamer.stock.model.StockInfo;

public class NotAMarketStockFilter implements StockFilter {
	public boolean doFilter(List<StockInfo> stockInfoList) {
		String stockCode = stockInfoList.get(0).getCode();
		return stockCode.startsWith("20") || stockCode.startsWith("90");
	}
}
