package com.ltw.dreamer.engine.filter.impl;

import java.util.List;

import com.ltw.dreamer.engine.filter.StockFilter;
import com.ltw.dreamer.stock.model.StockInfo;

public class PerIsTooHighFilter implements StockFilter {
	public boolean doFilter(List<StockInfo> stockInfoList) {
		StockInfo stockInfo = stockInfoList.get(0);
		if (stockInfo.getPer() == null) return true;
		return stockInfoList.get(0).getPer() > 80;
	}
}
