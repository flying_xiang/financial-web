package com.ltw.dreamer.engine.filter.impl;

import java.util.List;

import com.ltw.dreamer.engine.filter.StockFilter;
import com.ltw.dreamer.stock.model.StockInfo;

public class PriceIstooHighFilter implements StockFilter {
	public boolean doFilter(List<StockInfo> stockInfoList) {
		return stockInfoList.get(0).getCurPrice().doubleValue() > 30.00;
	}
}
