package com.ltw.dreamer.engine.index;

import java.util.List;

import com.ltw.dreamer.stock.model.StockInfo;

public interface StockIndex {
	public double calStockIndex(List<StockInfo> stockInfoList) throws Exception;
}
