package com.ltw.dreamer.engine.index.impl;

import java.util.Calendar;
import java.util.List;

import com.ltw.dreamer.common.util.DateUtil;
import com.ltw.dreamer.engine.index.StockIndex;
import com.ltw.dreamer.stock.model.StockInfo;

public class ChangeRateIndex implements StockIndex {
	public double calStockIndex(List<StockInfo> stockInfoList) throws Exception {
		if (stockInfoList == null) throw new Exception("计算指标出现错误，未得到正确指标值");
		if (stockInfoList.size() == 0) throw new Exception("计算指标出现错误，未得到正确指标值");
		double multiplyFactor = DateUtil.getMultiplyFactor(Calendar.getInstance().getTime());
		if (stockInfoList.get(0).getExchangeRate() * multiplyFactor < 10.00) throw new Exception("计算指标出现错误，未得到正确指标值");
		
		List<StockInfo> sampleStockInfoList = stockInfoList;
		if (30 < stockInfoList.size()) sampleStockInfoList = stockInfoList.subList(1, 30);
		double averageExchangeRate = calAverageExchangeRate(sampleStockInfoList);
		return (stockInfoList.get(0).getExchangeRate() * multiplyFactor - averageExchangeRate) / averageExchangeRate;
	}

	private double calAverageExchangeRate(List<StockInfo> sampleStockInfoList) {
		double sumExchangeRate = 0.00;
		for (StockInfo stockInfo : sampleStockInfoList)
			sumExchangeRate += stockInfo.getExchangeRate();
		return sumExchangeRate / sampleStockInfoList.size();
	}
	
}
