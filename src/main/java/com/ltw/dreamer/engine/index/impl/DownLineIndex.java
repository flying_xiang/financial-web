package com.ltw.dreamer.engine.index.impl;

import com.ltw.dreamer.common.util.DateUtil;
import com.ltw.dreamer.common.util.GraphicUtil;
import com.ltw.dreamer.engine.pojo.Line;
import com.ltw.dreamer.engine.pojo.Point;
import com.ltw.dreamer.stock.model.StockInfo;

public class DownLineIndex extends LineIndex {

	@Override
	protected Point createPoint(StockInfo stockInfo) {
		Point point = new Point();
		point.setX(DateUtil.getSequence(stockInfo.getDate()));
		point.setY(stockInfo.getLowPrice());
		return point;
	}
	@Override
	protected boolean checkPointInArea(Point point, Line line) {
		return GraphicUtil.pointIsUpperLine(point, line);
	}
	
	@Override
	protected boolean checkPointOutArea(Point currentPoint, Line line) {
		return GraphicUtil.pointIsBelowLine(currentPoint, line);
	}
	
	@Override
	protected boolean compareLine(Line orginal, Line newone) {
		if (newone == null) return false;
		if (orginal == null) return true;
		return orginal.getK() < newone.getK();
	}

	@Override
	protected double getDeltaOfStandardLine() {
		return 0.02;
	}

	@Override
	protected final int getMininumDuration() {
		return 15;
	}

	@Override
	protected int getSampleSize() {
		return 50;
	}
}
