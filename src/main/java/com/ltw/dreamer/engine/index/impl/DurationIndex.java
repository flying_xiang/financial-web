package com.ltw.dreamer.engine.index.impl;

import java.util.List;

import com.ltw.dreamer.engine.index.StockIndex;
import com.ltw.dreamer.stock.model.StockInfo;

public class DurationIndex implements StockIndex {
	public double calStockIndex(List<StockInfo> stockInfoList) {
		double topPrice = 1.00;
		double minPrice = Double.MAX_VALUE;
		int count = 0;
		for (count=0; count<stockInfoList.size(); count++) {
			double currentPrice = calCurrentPrice(stockInfoList.get(count));
			if (currentPrice > topPrice) topPrice = currentPrice;
			if (currentPrice < minPrice) minPrice = currentPrice;
			if (!checkArea(topPrice, minPrice)) break;
		}
		return count;
	}

	private double calCurrentPrice(StockInfo stockInfo) {
		return (stockInfo.getTopPrice() + stockInfo.getLowPrice())/2;
	}
	
	private boolean checkArea(double topPrice, double minPrice) {
		return (topPrice - minPrice) / minPrice < 0.15;
	}
}
