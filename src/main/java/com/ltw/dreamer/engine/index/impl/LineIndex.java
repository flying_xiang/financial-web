package com.ltw.dreamer.engine.index.impl;

import java.util.List;
import com.ltw.dreamer.common.util.GraphicUtil;
import com.ltw.dreamer.engine.index.StockIndex;
import com.ltw.dreamer.engine.pojo.Line;
import com.ltw.dreamer.engine.pojo.Point;
import com.ltw.dreamer.stock.model.StockInfo;

public abstract class LineIndex implements StockIndex {
	public double calStockIndex(List<StockInfo> stockInfoList) throws Exception {
		Line line = createStandardLine(stockInfoList);
		if (line != null) return line.getK();
		throw new Exception("计算指标出现错误，未得到正确指标值");
	}
	protected Line createStandardLine(List<StockInfo> stockInfoList) {
		List<StockInfo> sampleList = stockInfoList;
		int size = getSampleSize();
		if (size < stockInfoList.size()) sampleList = stockInfoList.subList(0, size);
		
		Line boudaryLine = createBoundaryLine(sampleList);
		if (checkStandardLine(sampleList, boudaryLine)) return boudaryLine;
		return null;
	}
	protected Line createBoundaryLine(List<StockInfo> stockInfoList) {
		Line result = null;
		for (int i=1; i<stockInfoList.size(); i++) {
			Point firstPoint = createPoint(stockInfoList.get(i));
			for (int j=getMininumDuration()+1; j<stockInfoList.size(); j++) {
				Point secondPoint = createPoint(stockInfoList.get(j));
				Line tempLine = GraphicUtil.createLine(firstPoint, secondPoint);
				if (!lineIsTheBoundary(stockInfoList, tempLine)) continue;
				if (compareLine(result, tempLine)) result = tempLine;
			}
		}
		return result;
	}
	private boolean lineIsTheBoundary(List<StockInfo> stockInfoList, Line tempLine) {
		for (int i=1; i<stockInfoList.size(); i++) {
			Point currentPoint = createPoint(stockInfoList.get(i));
			if (!checkPointInArea(currentPoint, tempLine)) return false;
		}
		return true;
	}
	protected boolean checkStandardLine(List<StockInfo> stockInfoList, Line line) {
		if (line == null) return false;

		if (!checkStandardLineRegressionTimes(stockInfoList, line)) return false;
		return true;
	}
	private boolean checkStandardLineRegressionTimes(List<StockInfo> stockInfoList, Line line) {
		int matchPointCnt = 0;
		for (int i=1; i<stockInfoList.size(); i++) {
			StockInfo stockInfo = stockInfoList.get(i);
			Point point = createPoint(stockInfo);
			if (checkPointOutArea(point, line, getDeltaOfStandardLine())) matchPointCnt++;
		}
		if (matchPointCnt >= 3) return true;
		return false;
	}	
	protected boolean checkPointOutArea(Point point, Line line, double delta) {
		return checkPointOutArea(point, GraphicUtil.createLine(line, delta));
	}
	protected abstract Point createPoint(StockInfo stockInfo);
	protected abstract boolean compareLine(Line orginal, Line newone);
	protected abstract boolean checkPointInArea(Point point, Line line);
	protected abstract boolean checkPointOutArea(Point currentPoint, Line line);
	protected abstract double getDeltaOfStandardLine();
	protected abstract int getMininumDuration();
	protected abstract int getSampleSize();
}
