package com.ltw.dreamer.engine.index.impl;

import org.apache.log4j.Logger;
import com.ltw.dreamer.common.util.DateUtil;
import com.ltw.dreamer.common.util.GraphicUtil;
import com.ltw.dreamer.engine.pojo.Line;
import com.ltw.dreamer.engine.pojo.Point;
import com.ltw.dreamer.stock.model.StockInfo;

public class UpperLineIndex extends LineIndex {
	public static final Logger LOGGER = Logger.getLogger(UpperLineIndex.class);

	@Override
	protected Point createPoint(StockInfo stockInfo) {
		Point point = new Point();
		point.setX(DateUtil.getSequence(stockInfo.getDate()));
		point.setY(stockInfo.getTopPrice());
		return point;
	}

	@Override
	protected boolean checkPointInArea(Point point, Line line) {
		return GraphicUtil.pointIsBelowLine(point, line);
	}
	@Override
	protected boolean checkPointOutArea(Point currentPoint, Line line) {
		return GraphicUtil.pointIsUpperLine(currentPoint, line);
	}
	
	@Override
	protected boolean compareLine(Line orginal, Line newone) {
		if (newone == null) return false;
		if (orginal == null) return true;
		return orginal.getK() > newone.getK();
	}

	@Override
	protected double getDeltaOfStandardLine() {
		return -0.02;
	}

	@Override
	protected final int getMininumDuration() {
		return 15; // 趋势线限制
	}

	@Override
	protected int getSampleSize() {
		return 50; // 样本容量
	}
}
