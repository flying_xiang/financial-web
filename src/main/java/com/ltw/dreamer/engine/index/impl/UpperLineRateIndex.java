package com.ltw.dreamer.engine.index.impl;

import java.util.List;
import com.ltw.dreamer.engine.pojo.Line;
import com.ltw.dreamer.engine.pojo.Point;
import com.ltw.dreamer.stock.model.StockInfo;

public class UpperLineRateIndex extends UpperLineIndex {
	public double calStockIndex(List<StockInfo> stockInfoList) throws Exception {
		Line line = createStandardLine(stockInfoList);
		if (line != null) return calRateValue(line, stockInfoList.get(0));
			
		throw new Exception("计算指标出现错误，未得到正确指标值");
	}

	private double calRateValue(Line line, StockInfo stockInfo) {
		Point point = createPoint(stockInfo);
		point.setY(stockInfo.getCurPrice());
		double standardY = line.getK() * point.getX() + line.getB();
		
		return (point.getY() - standardY) / standardY;
	}
}
