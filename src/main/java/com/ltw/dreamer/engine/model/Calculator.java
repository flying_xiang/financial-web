package com.ltw.dreamer.engine.model;

import java.util.Date;

public class Calculator {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column calculator.id
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Integer id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column calculator.code
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private String code;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column calculator.recommend_code
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private String recommendCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column calculator.weight
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Double weight;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column calculator.calculator_type
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private String calculatorType;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column calculator.weight_type
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private String weightType;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column calculator.description
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private String description;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column calculator.state
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Integer state;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column calculator.created_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Date createdDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column calculator.updated_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Date updatedDate;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column calculator.id
	 * @return  the value of calculator.id
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column calculator.id
	 * @param id  the value for calculator.id
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column calculator.code
	 * @return  the value of calculator.code
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public String getCode() {
		return code;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column calculator.code
	 * @param code  the value for calculator.code
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column calculator.recommend_code
	 * @return  the value of calculator.recommend_code
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public String getRecommendCode() {
		return recommendCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column calculator.recommend_code
	 * @param recommendCode  the value for calculator.recommend_code
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setRecommendCode(String recommendCode) {
		this.recommendCode = recommendCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column calculator.weight
	 * @return  the value of calculator.weight
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Double getWeight() {
		return weight;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column calculator.weight
	 * @param weight  the value for calculator.weight
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setWeight(Double weight) {
		this.weight = weight;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column calculator.calculator_type
	 * @return  the value of calculator.calculator_type
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public String getCalculatorType() {
		return calculatorType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column calculator.calculator_type
	 * @param calculatorType  the value for calculator.calculator_type
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setCalculatorType(String calculatorType) {
		this.calculatorType = calculatorType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column calculator.weight_type
	 * @return  the value of calculator.weight_type
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public String getWeightType() {
		return weightType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column calculator.weight_type
	 * @param weightType  the value for calculator.weight_type
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setWeightType(String weightType) {
		this.weightType = weightType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column calculator.description
	 * @return  the value of calculator.description
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column calculator.description
	 * @param description  the value for calculator.description
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column calculator.state
	 * @return  the value of calculator.state
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column calculator.state
	 * @param state  the value for calculator.state
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column calculator.created_date
	 * @return  the value of calculator.created_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column calculator.created_date
	 * @param createdDate  the value for calculator.created_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column calculator.updated_date
	 * @return  the value of calculator.updated_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column calculator.updated_date
	 * @param updatedDate  the value for calculator.updated_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}