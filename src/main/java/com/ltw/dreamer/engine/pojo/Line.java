package com.ltw.dreamer.engine.pojo;

public class Line {
	private double k;
	private double b;
	public double getK() {
		return k;
	}
	public void setK(double k) {
		this.k = k;
	}
	public double getB() {
		return b;
	}
	public void setB(double b) {
		this.b = b;
	}
}
