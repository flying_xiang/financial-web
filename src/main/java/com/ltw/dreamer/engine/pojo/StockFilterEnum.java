package com.ltw.dreamer.engine.pojo;

import java.util.List;

import org.apache.log4j.Logger;

import com.ltw.dreamer.engine.filter.StockFilter;
import com.ltw.dreamer.engine.filter.impl.CurrDecrease1Filter;
import com.ltw.dreamer.engine.filter.impl.CurrPriceGoingDownFilter;
import com.ltw.dreamer.engine.filter.impl.CurrPriceMaxFilter;
import com.ltw.dreamer.engine.filter.impl.DataNotEnoughFilter;
import com.ltw.dreamer.engine.filter.impl.Day5Increase15Filter;
import com.ltw.dreamer.engine.filter.impl.ExchageRateLess5Filter;
import com.ltw.dreamer.engine.filter.impl.FlowValueIsTooBigFilter;
import com.ltw.dreamer.engine.filter.impl.NoBoundaryStockFilter;
import com.ltw.dreamer.engine.filter.impl.Not300StockFilter;
import com.ltw.dreamer.engine.filter.impl.NotAMarketStockFilter;
import com.ltw.dreamer.engine.filter.impl.PerIsTooHighFilter;
import com.ltw.dreamer.engine.filter.impl.PerIsTooLowFilter;
import com.ltw.dreamer.engine.filter.impl.PriceIstooHighFilter;
import com.ltw.dreamer.stock.model.StockInfo;

public enum StockFilterEnum {
    DATA_NOT_ENOUGH("数据量少于分析最小值，至少需要有40天的数据", -4000.00, new DataNotEnoughFilter()),
    DAY_5_INCREASE_15("过去五个交易日，股价上涨了15%", -4100.00, new Day5Increase15Filter()),
    CURR_DECREASE_1("当天股价下跌1%", -4200.00, new CurrDecrease1Filter()),
    NO_BOUNDARY_STOCK("没有涨跌幅限制的股票", -4300.00, new NoBoundaryStockFilter()),
    PRICE_IS_TOO_HIGH("当前股票价格过高，目前系统最高股价限制为30块", -4400.00, new PriceIstooHighFilter()),
    CURR_PRICE_MAX("当前股票价格触及涨停价", -4500.00, new CurrPriceMaxFilter()),
    NOT_A_MARKET_STOCK("当前股票非A股市场可交易股票", -4600.00, new NotAMarketStockFilter()),
    FLOW_VALUE_IS_TOO_BIG("流通市值过大，当前系统流通市值限制为100亿", -4700.00, new FlowValueIsTooBigFilter()),
    CURR_PRICE_GOING_DOWN("当天股价出现下跌", -9000.00, new CurrPriceGoingDownFilter()),
    EXCHANGE_RATE_LESS_5("当天股票换手率低于5%", -9100.00, new ExchageRateLess5Filter()),
    NOT_300_STOCK("股票非创业板股票", -9200.00, new Not300StockFilter()),
    PER_IS_TOO_HIGH("股票市盈率过高，当前系统设置市盈率最高限制为80", -9300.00, new PerIsTooHighFilter()),
    PER_IS_TOO_LOW("股票市盈率过低，当前系统设置市盈率最低限制为15", -9400, new PerIsTooLowFilter());
	
	public static final Logger LOGGER = Logger.getLogger(StockFilterEnum.class);
	private String description;
	private double code;
	private StockFilter filter;
	private StockFilterEnum(String description, double code, StockFilter filter) {
		this.description = description;
		this.code = code;
		this.filter = filter;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getCode() {
		return code;
	}
	public void setCode(double code) {
		this.code = code;
	}
	public boolean doFilter(List<StockInfo> stockInfoList) {
		try {
			return filter.doFilter(stockInfoList);	
		} catch (Exception ex) {
			LOGGER.info("ִ执行过滤器方法时出现错误：" + ex.getLocalizedMessage());
			return true;
		}
	}
}
