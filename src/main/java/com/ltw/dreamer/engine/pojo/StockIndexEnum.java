package com.ltw.dreamer.engine.pojo;

import java.util.List;

import com.ltw.dreamer.engine.index.StockIndex;
import com.ltw.dreamer.engine.index.impl.ChangeRateIndex;
import com.ltw.dreamer.engine.index.impl.DownLineIndex;
import com.ltw.dreamer.engine.index.impl.DownLineRateIndex;
import com.ltw.dreamer.engine.index.impl.DurationIndex;
import com.ltw.dreamer.engine.index.impl.UpperLineIndex;
import com.ltw.dreamer.engine.index.impl.UpperLineRateIndex;
import com.ltw.dreamer.stock.model.StockInfo;

import common.Logger;

public enum StockIndexEnum {
    CHANGE_RATE_INDEX("换手率指标", 10, new ChangeRateIndex()),
    DURATION_INDEX("横盘持续时间指标", 20, new DurationIndex()),
    UPPER_LINE_INDEX("压力线指标", 30, new UpperLineIndex()),
    UPPER_LINE_RATE_INDEX("压力线突破度指标", 31, new UpperLineRateIndex()),
    DOWN_LINE_INDEX("支撑线指标", 40, new DownLineIndex()),
    DOWN_LINE_RATE_INDEX("支撑线突破度指标", 41, new DownLineRateIndex());
	
	public static final Logger LOGGER = Logger.getLogger(StockIndexEnum.class);
	public static final double INNER_ERROR = -9999.00;
	
	private String description;
	private int index;
	private StockIndex stockIndex;
	private StockIndexEnum(String description, int index, StockIndex stockIndex) {
		this.description = description;
		this.index = index;
		this.stockIndex = stockIndex;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public double calStockIndex(List<StockInfo> stockInfoList) {
		try {
			return stockIndex.calStockIndex(stockInfoList);	
		} catch (Exception ex) {
			return INNER_ERROR;
		}
	}
}
