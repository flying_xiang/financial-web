package com.ltw.dreamer.engine.service;

import java.util.List;

import com.ltw.dreamer.engine.model.Calculator;

public interface CalculatorService {

	public List<Calculator> queryCalculatorByRCode(String code);

}
