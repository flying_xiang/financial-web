package com.ltw.dreamer.engine.service;

import java.util.List;

import com.ltw.dreamer.engine.pojo.StockIndexEnum;
import com.ltw.dreamer.stock.model.StockInfo;

public interface StockAnalyseService {
	public boolean shouldAnalyse(List<StockInfo> stockInfoList);
	public double calStockIndex(StockIndexEnum index, List<StockInfo> stockInfoList);
}
