package com.ltw.dreamer.engine.service;

import com.ltw.dreamer.engine.model.WeightMatrix;

public interface WeightMatrixService {
	public WeightMatrix queryWeightMatrixByCCode(String code);
}
