package com.ltw.dreamer.engine.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ltw.dreamer.engine.mapper.CalculatorMapper;
import com.ltw.dreamer.engine.model.Calculator;
import com.ltw.dreamer.engine.model.CalculatorExample;
import com.ltw.dreamer.engine.model.CalculatorExample.Criteria;
import com.ltw.dreamer.engine.service.CalculatorService;

@Service
public class CalculatorServiceImpl implements CalculatorService {
	@Resource
	private CalculatorMapper mapper = null;
	
	public List<Calculator> queryCalculatorByRCode(String code) {
		if (code == null || code.trim().length() == 0) throw new IllegalArgumentException("��δ���");
		CalculatorExample example = new CalculatorExample();
		Criteria criteria = example.createCriteria();
		criteria.andRecommendCodeEqualTo(code);
		
		return mapper.selectByExample(example);
	}
}
