package com.ltw.dreamer.engine.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ltw.dreamer.engine.core.StockWatchDog;
import com.ltw.dreamer.engine.pojo.StockIndexEnum;
import com.ltw.dreamer.engine.service.StockAnalyseService;
import com.ltw.dreamer.stock.model.StockInfo;

import common.Logger;

@Service
public class StockAnalyseServiceImpl implements StockAnalyseService {
	public static final Logger LOGGER = Logger.getLogger(StockAnalyseServiceImpl.class);
	
	@Resource
	private StockWatchDog watchDog = null;
	
	public boolean shouldAnalyse(List<StockInfo> stockInfoList) {
		return watchDog.shouldAnalyse(stockInfoList);
	}
	
	public double calStockIndex(StockIndexEnum index, List<StockInfo> stockInfoList) {
		return index.calStockIndex(stockInfoList);
	}
}
