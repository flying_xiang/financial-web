package com.ltw.dreamer.engine.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ltw.dreamer.engine.mapper.WeightMatrixMapper;
import com.ltw.dreamer.engine.model.WeightMatrix;
import com.ltw.dreamer.engine.model.WeightMatrixExample;
import com.ltw.dreamer.engine.model.WeightMatrixExample.Criteria;
import com.ltw.dreamer.engine.service.WeightMatrixService;

@Service
public class WeightMatrixServiceImpl implements WeightMatrixService {
	@Resource
	private WeightMatrixMapper mapper = null;
	
	public WeightMatrix queryWeightMatrixByCCode(String code) {
		if (code == null) throw new IllegalArgumentException("��δ���");
		WeightMatrixExample example = new WeightMatrixExample();
		Criteria criteria = example.createCriteria();
		criteria.andCalculatorCodeEqualTo(code);
		
		List<WeightMatrix> weightMatrixList = mapper.selectByExample(example);
		if (weightMatrixList == null || weightMatrixList.size() == 0) return null;
		
		return weightMatrixList.get(0);
	}

}
