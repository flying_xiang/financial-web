package com.ltw.dreamer.report.exception;

public class FinancialReportException extends Exception{
	private static final long serialVersionUID = 5053345311621526936L;
	
	public FinancialReportException(String description) {
		super(description);
	}
}
