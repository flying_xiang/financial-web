package com.ltw.dreamer.report.service;

import com.ltw.dreamer.stock.model.Stock;

public interface BalanceSheetService {
	public void updateBalanceSheet(Stock stock);
}
