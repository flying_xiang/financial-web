package com.ltw.dreamer.report.service;

import com.ltw.dreamer.stock.model.Stock;

public interface CashFlowService {
	public void updateCashFlow(Stock stock);
}
