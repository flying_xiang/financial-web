package com.ltw.dreamer.report.service;

import com.ltw.dreamer.stock.model.Stock;

public interface ProfitStatementService {
	public void updateProfitStatement(Stock stock);
}
