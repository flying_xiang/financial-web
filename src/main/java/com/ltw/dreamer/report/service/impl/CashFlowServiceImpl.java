package com.ltw.dreamer.report.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import com.ltw.dreamer.common.network.WebPageReader;
import com.ltw.dreamer.common.util.DateUtil;
import com.ltw.dreamer.common.util.VerticalTableUtil;
import com.ltw.dreamer.report.exception.FinancialReportException;
import com.ltw.dreamer.report.mapper.CashFlowMapper;
import com.ltw.dreamer.report.model.CashFlow;
import com.ltw.dreamer.report.model.CashFlowExample;
import com.ltw.dreamer.report.model.CashFlowExample.Criteria;
import com.ltw.dreamer.report.service.CashFlowService;
import com.ltw.dreamer.report.service.FinancialReportDefineService;
import com.ltw.dreamer.stock.model.Stock;

@Service
public class CashFlowServiceImpl implements CashFlowService {
	public static final Logger LOGGER = Logger.getLogger(CashFlowService.class);
	public static final String PAGE_URI = "http://money.finance.sina.com.cn/corp/go.php/vFD_CashFlow/stockid/STOCK_CODE/ctrl/YEAR/displaytype/4.phtml";
	public static final String REPORT_NAME = "现金流量表";

	@Resource
	private WebPageReader reader = null;
	
	@Resource
	private CashFlowMapper mapper = null;
	
	@Resource
	private FinancialReportDefineService financialReportDefineService = null;
	
	public void updateCashFlow(Stock stock) {
		if (stock == null) throw new IllegalArgumentException("传入股票参数为空。");
		LOGGER.info("开始更新股票" + stock.getName() + "[" + stock.getCode() + "]现金流量表信息>>>>");
		boolean noStop = true;
		int year = DateUtil.getCurrentYear();
		while (noStop) {
			try {
				noStop = updateCashFlow(stock, year--);
			} catch (FinancialReportException e) {
				LOGGER.warn(e.getLocalizedMessage());
			}
		}
		LOGGER.info("结束更新股票" + stock.getName() + "[" + stock.getCode() + "]现金流量表信息>>>>");
	}
	private boolean updateCashFlow(Stock stock, int year) throws FinancialReportException {
		LOGGER.info("处理年份>>>>" + year);
		String pageUri = PAGE_URI.replaceFirst("STOCK_CODE", stock.getCode()).replaceFirst("YEAR", Integer.toString(year));
		String content = reader.read(pageUri);
		if (content == null) {
			LOGGER.info("HTML文件读取错误，不再抓取本股票的数据：" + stock.getCode());
			return false;
		}
		Document doc = Jsoup.parse(content);
		Element element = doc.getElementById("ProfitStatementNewTable0");
		if (element == null) {
			LOGGER.info("HTML文件解析错误，不再抓取本股票的数据：" + stock.getCode());
			return false;
		}
		Elements allRows = element.getElementsByTag("tbody").get(0).getElementsByTag("tr");
		if (allRows == null || allRows.size() == 0) {
			LOGGER.info("现金流量表数据为空，股票代码：" + stock.getCode());
			return true; // 可能是数据临时存在问题，再往前爬一年看看
		}
		List<CashFlow> cashFlowList = createCashFlowList(stock, allRows);
		
		for (CashFlow cashFlow : cashFlowList)
			insertCashFlow(cashFlow);
		
		return true;
	}
	private List<CashFlow> createCashFlowList(Stock stock, Elements allRows) throws FinancialReportException {
		
		int index = findReportDateRowIndex(allRows);
		if (index == -1) {
			throw new FinancialReportException("现金流量表没有定义报表日期!");
		}
		
		List<CashFlow> cashFlowList = new ArrayList<CashFlow>();
		List<Date> reportDateList = getReportDateList(allRows.get(index));
		boolean needStore = false;
		for (int i=0; i<reportDateList.size(); i++) {
			CashFlow cashFlow = new CashFlow();
			cashFlow.setCode(stock.getCode());
			cashFlow.setCreatedDate(Calendar.getInstance().getTime());
			cashFlow.setUpdatedDate(Calendar.getInstance().getTime());
			cashFlow.setDate(reportDateList.get(i));
			
			if (!cashFlowIsStoredAlready(cashFlow)) needStore = true;
			
			cashFlowList.add(cashFlow);
		}
		if (!needStore) return new ArrayList<CashFlow>();
		
		index++;
		while (index < allRows.size())
			updateCashFlowList(cashFlowList, allRows.get(index++));
		
		return cashFlowList;
	}
	private int findReportDateRowIndex(Elements allRows) throws FinancialReportException {
		for (int i=0; i<allRows.size(); i++)
		{
			String rowdata = allRows.get(i).text();
			if (rowdata.indexOf("报表日期") != -1)
				return i;
			if (rowdata.indexOf("报告期") != -1)
				return i;
		}
		return -1;
	}
	private List<Date> getReportDateList(Element element) throws FinancialReportException {
		String text = element.text();
		String[] items = text.split("\\s");
		List<Date> reportDateList = new ArrayList<Date>();
		
		if (!items[0].equals("报表日期") && !items[0].equals("报告期"))
			throw new FinancialReportException("数据出现异常，报表日期配置有误，日期：" + text);

		try {
			for (int i=1; i<items.length; i++) {
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(items[i]);
				reportDateList.add(date);
			}
		} catch (ParseException e) {
			throw new FinancialReportException("现金流量表日期格式化时出现错误，日期：" + text);
		}
		
		return reportDateList;
	}
	private boolean cashFlowIsStoredAlready(CashFlow cashFlow) {
		CashFlowExample example = new CashFlowExample();
		Criteria criteria = example.createCriteria();
		criteria.andCodeEqualTo(cashFlow.getCode());
		criteria.andDateEqualTo(cashFlow.getDate());
		List<CashFlow> cashFlowList = mapper.selectByExample(example);
		if (cashFlowList != null && cashFlowList.size() != 0) {
			LOGGER.info("当前股票的现金流量表数据已经存在, 股票代码：" + cashFlow.getCode() + ", 日期：" + cashFlow.getDate());
			return true;
		}
		
		return false;
	}
	private void updateCashFlowList(List<CashFlow> cashFlowList, Element element) {
		String text = element.text();
		String[] items = text.split("\\s");
		if (!validateData(cashFlowList, items)) {
			LOGGER.info("当前行的数据不符合规范要求，不做处理，text=" + text);
			return ;
		}
		
		String code = items[0].trim();
		if (code.length() == 0) return ;
		
		int index = financialReportDefineService.queryColumnIndex(REPORT_NAME, code);
		for (int i=0; i<cashFlowList.size(); i++)
			VerticalTableUtil.invokePropertySetter(cashFlowList.get(i), items[i+1], index);
	}
	private boolean validateData(List<CashFlow> cashFlowList, String[] items) {
		if (items == null || cashFlowList == null) return false;
		if (items.length != cashFlowList.size() + 1) return false;
		boolean valid = false;
		for (int i=1; i<items.length; i++) {
			try {
				Double.parseDouble(items[i].replaceAll(",", ""));
				valid = true;
			} catch (NumberFormatException ex) {
				// LOGGER.info("当前行记录存在数据不规范, text=" + items[i]);
			}
		}
		return valid;
	}
	private void insertCashFlow(CashFlow cashFlow) {
		mapper.insert(cashFlow);
	}
}
