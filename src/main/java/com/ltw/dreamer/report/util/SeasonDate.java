package com.ltw.dreamer.report.util;

import com.ltw.dreamer.common.util.DateUtil;

public class SeasonDate {
	private int year;
	private int season;
	public SeasonDate() {
		this.year = DateUtil.getCurrentYear();
		this.season = DateUtil.getCurrentSeason();
	}
	
	public void decrease() {
		if (season == 1) {
			year--;
			season = 4;
			return ;
		}
		
		season--;
	}
	
	public int getYear() {
		return this.year;
	}
	public int getSeason() {
		return this.season;
	}
}