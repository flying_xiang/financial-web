package com.ltw.dreamer.sms.service;

import com.ltw.dreamer.sms.pojo.MailBean;

public interface SMTPService {
	 public boolean sendMail(MailBean mb);
}