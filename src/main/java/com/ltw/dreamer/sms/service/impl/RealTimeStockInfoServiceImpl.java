package com.ltw.dreamer.sms.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ltw.dreamer.common.util.DateUtil;
import com.ltw.dreamer.sms.pojo.MailBean;
import com.ltw.dreamer.sms.service.RealTimeStockInfoService;
import com.ltw.dreamer.sms.service.SMTPService;
import com.ltw.dreamer.sms.util.MailUtil;
import com.ltw.dreamer.stock.model.Stock;
import com.ltw.dreamer.stock.service.StockService;

@Service
public class RealTimeStockInfoServiceImpl implements RealTimeStockInfoService {
	@Resource
	private SMTPService smtpService = null;
	
	@Resource
	private StockService stockService = null;
	
	private String[] mailDestArray = new String[]{"wangzhibin091@pingan.com.cn", "xiang_seu@163.com", "flyingbryant@163.com"};
	
	public void notifyRealTimeStockInfo() {
		MailBean mailBean = createMailBean();
		
		if (mailDestArray == null) return ;
		for (String destination : mailDestArray)
			this.sendmail(mailBean, destination);
	}
	
	private void sendmail(MailBean content, String destination) {
		smtpService.sendMail(content);
		
		// 发送到另一个工作邮箱
		content.setTo(destination);
		smtpService.sendMail(content);	
	}

	private MailBean createMailBean() {
		MailBean mailBean = MailUtil.createMailBean();
        mailBean.setSubject(createMailSubject());
		List<Stock> stockList = queryAllStock();
		List<Stock> stockOfLatestDate = pickUpLatestStockList(stockList);
		
		List<Stock> stockUpperIndexList = pickUpUpperIndexStockList(stockOfLatestDate);
		String contentOfUpperIndexStock = createContentOfUpperIndexStock(stockUpperIndexList);
		
//		List<Stock> stockDownIndexList = pickUpDownIndexStockList(stockOfLatestDate);
//		String contentOfDownIndexStock = createContentOfDownIndexStock(stockDownIndexList);
		
		mailBean.setContent(contentOfUpperIndexStock); 
        return mailBean;
	}

	private String createContentOfUpperIndexStock(List<Stock> stockUpperIndexList) {
		StringBuffer sb = new StringBuffer();
		sb.append("一、处于向上突破的股票清单: \r\n");
		sb.append("<table border=\"1\">");
		sb.append("\t<tr>");
		sb.append("\t\t<th>股票代码</th>");
		sb.append("\t\t<th>股票名称</th>");
		sb.append("\t\t<th>股票市场</th>");
		sb.append("\t\t<th>当前价格</th>");
		sb.append("\t\t<th>日期</th>");
		sb.append("\t\t<th>平盘指标</th>");
		sb.append("\t\t<th>量价指标</th>");
		sb.append("\t\t<th>压力线指标</th>");
		sb.append("\t\t<th>压力线突破</th>");
		sb.append("\t\t<th>支撑线指标</th>");
		sb.append("\t\t<th>支撑线突破</th>");
		sb.append("\t</tr>");
		for (Stock stock : stockUpperIndexList)
			sb.append(createStockHtmlInfo(stock));
		sb.append("</table>");
		return sb.toString();
	}
	
	private List<Stock> pickUpLatestStockList(List<Stock> stockList) {
		String latestDateStr = findLatestDateStr(stockList);
		List<Stock> stockOfLatestDate = new ArrayList<Stock>();
		
		for (Stock stock : stockList) {
			String stockDateStr = DateUtil.getDailyDateString(stock.getDate());
			if (stockDateStr.compareTo(latestDateStr) != 0) continue; 
			if (stock.getName().toUpperCase().indexOf("ST") != -1) continue;
			if (stock.getTotalValues() < -100.00) continue;
			// if (stock.getFlowValues() > 100.00) continue;
			if (stock.getConfidence() == null) continue;
			// if (stock.getCurrentPrice() > 30.00) continue;
			stockOfLatestDate.add(stock);
		}
		return stockOfLatestDate;
	}
	private List<Stock> pickUpUpperIndexStockList(List<Stock> stockOfLatestDate) {
		List<Stock> upperIndexStockList = new ArrayList<Stock>();
		
		for (Stock stock : stockOfLatestDate) {		
			if (stock.getPriceRatio() == null) continue;
			if (stock.getPriceRatio() < -1000) continue;
			if (stock.getCurrentRatio() < 0.00) continue;
			upperIndexStockList.add(stock);
		}
		return upperIndexStockList;
	}

	private List<Stock> queryAllStock() {
		return stockService.queryAllStock();
	}
	
	private String findLatestDateStr(List<Stock> stockList) {
		String latestDateStr = "2015-01-01";
		for (Stock stock : stockList) {
			String stockDate = DateUtil.getDailyDateString(stock.getDate());
			if (stockDate.compareTo(latestDateStr) > 0) latestDateStr = stockDate;
		}

		return latestDateStr;
	}

	private String createMailSubject() {
		return "股票实时价格统计情况（" + DateUtil.getCurrentDateString() + "）";
	}

	private String createStockHtmlInfo(Stock stock) {
		StringBuffer sb = new StringBuffer();
		sb.append("\t<tr>");
		sb.append("\t\t<th>" + stock.getCode() + "</th>");
		sb.append("\t\t<th>" + stock.getName() + "</th>");
		sb.append("\t\t<th>" + stock.getMarket() + "</th>");
		sb.append("\t\t<th>" + stock.getCurrentPrice() + "</th>");
		sb.append("\t\t<th>" + DateUtil.getDailyDateString(stock.getDate()) + "</th>");
		sb.append("\t\t<th>" + stock.getConfidence() + "</th>");
		sb.append("\t\t<th>" + stock.getTotalValues() + "</th>");
		sb.append("\t\t<th>" + cleanIndexValue(stock.getPriceRatio()) + "</th>");
		sb.append("\t\t<th>" + cleanIndexValue(stock.getCurrentRatio()) + "</th>");
		sb.append("\t\t<th>" + cleanIndexValue(stock.getMinPrice()) + "</th>");
		sb.append("\t\t<th>" + cleanIndexValue(stock.getMaxPrice()) + "</th>");
		sb.append("\t</tr>");
		return sb.toString();
	}
	
	private String cleanIndexValue(double index) {
		if (index == -9999) return "-";
		return Double.toString(index);
	}
}
