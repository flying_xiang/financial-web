package com.ltw.dreamer.sms.service.impl;

import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.springframework.stereotype.Service;

import com.ltw.dreamer.sms.pojo.MailBean;
import com.ltw.dreamer.sms.service.SMTPService;

@Service
public class SMTPServiceImpl implements SMTPService {
    public boolean sendMail(MailBean mb) {
        String host = mb.getHost();
        final String username = mb.getUsername();
        final String password = mb.getPassword();
        String from = mb.getFrom();
        String to = mb.getTo();
        String subject = mb.getSubject();
        String content = mb.getContent();
        Vector<String> file = mb.getFile();
        
        
        Properties props = System.getProperties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", "true");
        
        Session session = Session.getInstance(props, new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(toChinese(subject, "GB2312"));

            Multipart mp = new MimeMultipart();
            MimeBodyPart mbpContent = new MimeBodyPart();
            mbpContent.setContent(content, "text/html;charset=GB2312");
            mp.addBodyPart(mbpContent);

            if (file != null) addAppendix(mp, file);
            
            msg.setContent(mp);
            msg.setSentDate(new Date());
            Transport.send(msg);
        } catch (MessagingException me) {
            me.printStackTrace();
            return false;
        }
        return true;
    }
    
    private void addAppendix(Multipart mp, Vector<String> file) throws MessagingException {
        Enumeration<String> efile = file.elements();
        while (efile.hasMoreElements()) {
            MimeBodyPart mbpFile = new MimeBodyPart();
            String fileName = efile.nextElement().toString();
            FileDataSource fds = new FileDataSource(fileName);
            mbpFile.setDataHandler(new DataHandler(fds));
            mbpFile.setFileName(toChinese(fds.getName(), "UTF-8"));
            mp.addBodyPart(mbpFile);
        }
    }
    
    private String toChinese(String text, String encode) {
        try {
            text = MimeUtility.encodeText(new String(text.getBytes(), "GB2312"), encode, "B");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }
}