package com.ltw.dreamer.sms.util;

import java.util.Date;
import java.util.Properties;
import java.util.Vector;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import com.ltw.dreamer.sms.pojo.MailBean;

public class MailUtil {
	public static MailBean createMailBean() {
		MailBean mailBean = new MailBean();
		
		mailBean.setHost("smtp.126.com");
		mailBean.setUsername("wangzhibin2013");
		mailBean.setPassword("201314");
		mailBean.setFrom("wangzhibin2013@126.com");
		mailBean.setTo("stockltw@foxmail.com");
		
		return mailBean;
	}
	
    public static boolean sendMail(final MailBean mailBean) {
        try {
        	MimeMessage mimeMessage = createMimeMessage(mailBean);
            mimeMessage.setContent(createMultipart(mailBean));
            
            Transport.send(mimeMessage);
        } catch (MessagingException me) {
            me.printStackTrace();
            return false;
        }
        return true;
    }

	private static MimeMessage createMimeMessage(final MailBean mailBean) throws MessagingException, AddressException {
    	Session session = createSession(mailBean);
        MimeMessage mimeMessage = new MimeMessage(session);
        
		mimeMessage.setFrom(new InternetAddress(mailBean.getFrom()));
		mimeMessage.setRecipients(Message.RecipientType.TO, new InternetAddress[] {new InternetAddress(mailBean.getTo())});
		mimeMessage.setSubject(toChinese(mailBean.getSubject(), "GB2312"));
		mimeMessage.setSentDate(new Date());
		
		return mimeMessage;
	}
	
	private static Session createSession(final MailBean mailBean) {
		Properties props = System.getProperties();
        props.put("mail.smtp.host", mailBean.getHost());  // 设置SMTP的主机
        props.put("mail.smtp.auth", "true");       // 需要经过验证
        
        Session session = Session.getInstance(props, new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailBean.getUsername(), mailBean.getPassword());
            }
        });
        
		return session;
	}

	private static Multipart createMultipart(final MailBean mailBean) throws MessagingException {
		Multipart multipart = new MimeMultipart();
		
		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		multipart.addBodyPart(mimeBodyPart);
		
		mimeBodyPart.setText(mailBean.getContent());
		
		Vector<String> fileList = mailBean.getFile();
    	if (fileList == null) return multipart;
    	
    	for (String fileName : fileList)
    		multipart.addBodyPart(createAppendix(fileName));
		
		return multipart;
	}

	private static MimeBodyPart createAppendix(String fileName) throws MessagingException {
		MimeBodyPart appendix = new MimeBodyPart();
		
		FileDataSource fds = new FileDataSource(fileName);
		appendix.setDataHandler(new DataHandler(fds));
		appendix.setFileName(toChinese(fds.getName(), "UTF-8"));
		
		return appendix;
	}
	
    private static String toChinese(String text, String encode) {
        try {
            text = MimeUtility.encodeText(new String(text.getBytes(), "GB2312"), encode, "B");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }
}