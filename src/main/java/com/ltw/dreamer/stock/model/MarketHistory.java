package com.ltw.dreamer.stock.model;

import java.util.Date;

public class MarketHistory {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.id
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Integer id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.code
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private String code;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Date date;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.created_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Date createdDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.updated_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Date updatedDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.open_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Double openPrice;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.max_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Double maxPrice;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.close_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Double closePrice;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.min_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Double minPrice;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.amount
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Double amount;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.count
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Double count;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column market_history.weight
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	private Double weight;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.id
	 * @return  the value of market_history.id
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.id
	 * @param id  the value for market_history.id
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.code
	 * @return  the value of market_history.code
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public String getCode() {
		return code;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.code
	 * @param code  the value for market_history.code
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.date
	 * @return  the value of market_history.date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.date
	 * @param date  the value for market_history.date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.created_date
	 * @return  the value of market_history.created_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.created_date
	 * @param createdDate  the value for market_history.created_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.updated_date
	 * @return  the value of market_history.updated_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.updated_date
	 * @param updatedDate  the value for market_history.updated_date
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.open_price
	 * @return  the value of market_history.open_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Double getOpenPrice() {
		return openPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.open_price
	 * @param openPrice  the value for market_history.open_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setOpenPrice(Double openPrice) {
		this.openPrice = openPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.max_price
	 * @return  the value of market_history.max_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Double getMaxPrice() {
		return maxPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.max_price
	 * @param maxPrice  the value for market_history.max_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.close_price
	 * @return  the value of market_history.close_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Double getClosePrice() {
		return closePrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.close_price
	 * @param closePrice  the value for market_history.close_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setClosePrice(Double closePrice) {
		this.closePrice = closePrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.min_price
	 * @return  the value of market_history.min_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Double getMinPrice() {
		return minPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.min_price
	 * @param minPrice  the value for market_history.min_price
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.amount
	 * @return  the value of market_history.amount
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.amount
	 * @param amount  the value for market_history.amount
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.count
	 * @return  the value of market_history.count
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Double getCount() {
		return count;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.count
	 * @param count  the value for market_history.count
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setCount(Double count) {
		this.count = count;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column market_history.weight
	 * @return  the value of market_history.weight
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Double getWeight() {
		return weight;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column market_history.weight
	 * @param weight  the value for market_history.weight
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setWeight(Double weight) {
		this.weight = weight;
	}
}