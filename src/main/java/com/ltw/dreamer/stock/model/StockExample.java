package com.ltw.dreamer.stock.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class StockExample {
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public StockExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		protected void addCriterionForJDBCDate(String condition, Date value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			addCriterion(condition, new java.sql.Date(value.getTime()), property);
		}

		protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
			if (values == null || values.size() == 0) {
				throw new RuntimeException("Value list for " + property + " cannot be null or empty");
			}
			List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
			Iterator<Date> iter = values.iterator();
			while (iter.hasNext()) {
				dateList.add(new java.sql.Date(iter.next().getTime()));
			}
			addCriterion(condition, dateList, property);
		}

		protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
		}

		public Criteria andCodeIsNull() {
			addCriterion("code is null");
			return (Criteria) this;
		}

		public Criteria andCodeIsNotNull() {
			addCriterion("code is not null");
			return (Criteria) this;
		}

		public Criteria andCodeEqualTo(String value) {
			addCriterion("code =", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeNotEqualTo(String value) {
			addCriterion("code <>", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeGreaterThan(String value) {
			addCriterion("code >", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeGreaterThanOrEqualTo(String value) {
			addCriterion("code >=", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeLessThan(String value) {
			addCriterion("code <", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeLessThanOrEqualTo(String value) {
			addCriterion("code <=", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeLike(String value) {
			addCriterion("code like", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeNotLike(String value) {
			addCriterion("code not like", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeIn(List<String> values) {
			addCriterion("code in", values, "code");
			return (Criteria) this;
		}

		public Criteria andCodeNotIn(List<String> values) {
			addCriterion("code not in", values, "code");
			return (Criteria) this;
		}

		public Criteria andCodeBetween(String value1, String value2) {
			addCriterion("code between", value1, value2, "code");
			return (Criteria) this;
		}

		public Criteria andCodeNotBetween(String value1, String value2) {
			addCriterion("code not between", value1, value2, "code");
			return (Criteria) this;
		}

		public Criteria andNameIsNull() {
			addCriterion("name is null");
			return (Criteria) this;
		}

		public Criteria andNameIsNotNull() {
			addCriterion("name is not null");
			return (Criteria) this;
		}

		public Criteria andNameEqualTo(String value) {
			addCriterion("name =", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotEqualTo(String value) {
			addCriterion("name <>", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThan(String value) {
			addCriterion("name >", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThanOrEqualTo(String value) {
			addCriterion("name >=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThan(String value) {
			addCriterion("name <", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThanOrEqualTo(String value) {
			addCriterion("name <=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLike(String value) {
			addCriterion("name like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotLike(String value) {
			addCriterion("name not like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameIn(List<String> values) {
			addCriterion("name in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotIn(List<String> values) {
			addCriterion("name not in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameBetween(String value1, String value2) {
			addCriterion("name between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotBetween(String value1, String value2) {
			addCriterion("name not between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andMarketIsNull() {
			addCriterion("market is null");
			return (Criteria) this;
		}

		public Criteria andMarketIsNotNull() {
			addCriterion("market is not null");
			return (Criteria) this;
		}

		public Criteria andMarketEqualTo(String value) {
			addCriterion("market =", value, "market");
			return (Criteria) this;
		}

		public Criteria andMarketNotEqualTo(String value) {
			addCriterion("market <>", value, "market");
			return (Criteria) this;
		}

		public Criteria andMarketGreaterThan(String value) {
			addCriterion("market >", value, "market");
			return (Criteria) this;
		}

		public Criteria andMarketGreaterThanOrEqualTo(String value) {
			addCriterion("market >=", value, "market");
			return (Criteria) this;
		}

		public Criteria andMarketLessThan(String value) {
			addCriterion("market <", value, "market");
			return (Criteria) this;
		}

		public Criteria andMarketLessThanOrEqualTo(String value) {
			addCriterion("market <=", value, "market");
			return (Criteria) this;
		}

		public Criteria andMarketLike(String value) {
			addCriterion("market like", value, "market");
			return (Criteria) this;
		}

		public Criteria andMarketNotLike(String value) {
			addCriterion("market not like", value, "market");
			return (Criteria) this;
		}

		public Criteria andMarketIn(List<String> values) {
			addCriterion("market in", values, "market");
			return (Criteria) this;
		}

		public Criteria andMarketNotIn(List<String> values) {
			addCriterion("market not in", values, "market");
			return (Criteria) this;
		}

		public Criteria andMarketBetween(String value1, String value2) {
			addCriterion("market between", value1, value2, "market");
			return (Criteria) this;
		}

		public Criteria andMarketNotBetween(String value1, String value2) {
			addCriterion("market not between", value1, value2, "market");
			return (Criteria) this;
		}

		public Criteria andMarketCodeIsNull() {
			addCriterion("market_code is null");
			return (Criteria) this;
		}

		public Criteria andMarketCodeIsNotNull() {
			addCriterion("market_code is not null");
			return (Criteria) this;
		}

		public Criteria andMarketCodeEqualTo(String value) {
			addCriterion("market_code =", value, "marketCode");
			return (Criteria) this;
		}

		public Criteria andMarketCodeNotEqualTo(String value) {
			addCriterion("market_code <>", value, "marketCode");
			return (Criteria) this;
		}

		public Criteria andMarketCodeGreaterThan(String value) {
			addCriterion("market_code >", value, "marketCode");
			return (Criteria) this;
		}

		public Criteria andMarketCodeGreaterThanOrEqualTo(String value) {
			addCriterion("market_code >=", value, "marketCode");
			return (Criteria) this;
		}

		public Criteria andMarketCodeLessThan(String value) {
			addCriterion("market_code <", value, "marketCode");
			return (Criteria) this;
		}

		public Criteria andMarketCodeLessThanOrEqualTo(String value) {
			addCriterion("market_code <=", value, "marketCode");
			return (Criteria) this;
		}

		public Criteria andMarketCodeLike(String value) {
			addCriterion("market_code like", value, "marketCode");
			return (Criteria) this;
		}

		public Criteria andMarketCodeNotLike(String value) {
			addCriterion("market_code not like", value, "marketCode");
			return (Criteria) this;
		}

		public Criteria andMarketCodeIn(List<String> values) {
			addCriterion("market_code in", values, "marketCode");
			return (Criteria) this;
		}

		public Criteria andMarketCodeNotIn(List<String> values) {
			addCriterion("market_code not in", values, "marketCode");
			return (Criteria) this;
		}

		public Criteria andMarketCodeBetween(String value1, String value2) {
			addCriterion("market_code between", value1, value2, "marketCode");
			return (Criteria) this;
		}

		public Criteria andMarketCodeNotBetween(String value1, String value2) {
			addCriterion("market_code not between", value1, value2, "marketCode");
			return (Criteria) this;
		}

		public Criteria andCreatedDateIsNull() {
			addCriterion("created_date is null");
			return (Criteria) this;
		}

		public Criteria andCreatedDateIsNotNull() {
			addCriterion("created_date is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedDateEqualTo(Date value) {
			addCriterion("created_date =", value, "createdDate");
			return (Criteria) this;
		}

		public Criteria andCreatedDateNotEqualTo(Date value) {
			addCriterion("created_date <>", value, "createdDate");
			return (Criteria) this;
		}

		public Criteria andCreatedDateGreaterThan(Date value) {
			addCriterion("created_date >", value, "createdDate");
			return (Criteria) this;
		}

		public Criteria andCreatedDateGreaterThanOrEqualTo(Date value) {
			addCriterion("created_date >=", value, "createdDate");
			return (Criteria) this;
		}

		public Criteria andCreatedDateLessThan(Date value) {
			addCriterion("created_date <", value, "createdDate");
			return (Criteria) this;
		}

		public Criteria andCreatedDateLessThanOrEqualTo(Date value) {
			addCriterion("created_date <=", value, "createdDate");
			return (Criteria) this;
		}

		public Criteria andCreatedDateIn(List<Date> values) {
			addCriterion("created_date in", values, "createdDate");
			return (Criteria) this;
		}

		public Criteria andCreatedDateNotIn(List<Date> values) {
			addCriterion("created_date not in", values, "createdDate");
			return (Criteria) this;
		}

		public Criteria andCreatedDateBetween(Date value1, Date value2) {
			addCriterion("created_date between", value1, value2, "createdDate");
			return (Criteria) this;
		}

		public Criteria andCreatedDateNotBetween(Date value1, Date value2) {
			addCriterion("created_date not between", value1, value2, "createdDate");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateIsNull() {
			addCriterion("updated_date is null");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateIsNotNull() {
			addCriterion("updated_date is not null");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateEqualTo(Date value) {
			addCriterion("updated_date =", value, "updatedDate");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateNotEqualTo(Date value) {
			addCriterion("updated_date <>", value, "updatedDate");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateGreaterThan(Date value) {
			addCriterion("updated_date >", value, "updatedDate");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateGreaterThanOrEqualTo(Date value) {
			addCriterion("updated_date >=", value, "updatedDate");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateLessThan(Date value) {
			addCriterion("updated_date <", value, "updatedDate");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateLessThanOrEqualTo(Date value) {
			addCriterion("updated_date <=", value, "updatedDate");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateIn(List<Date> values) {
			addCriterion("updated_date in", values, "updatedDate");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateNotIn(List<Date> values) {
			addCriterion("updated_date not in", values, "updatedDate");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateBetween(Date value1, Date value2) {
			addCriterion("updated_date between", value1, value2, "updatedDate");
			return (Criteria) this;
		}

		public Criteria andUpdatedDateNotBetween(Date value1, Date value2) {
			addCriterion("updated_date not between", value1, value2, "updatedDate");
			return (Criteria) this;
		}

		public Criteria andMaxPriceIsNull() {
			addCriterion("max_price is null");
			return (Criteria) this;
		}

		public Criteria andMaxPriceIsNotNull() {
			addCriterion("max_price is not null");
			return (Criteria) this;
		}

		public Criteria andMaxPriceEqualTo(Double value) {
			addCriterion("max_price =", value, "maxPrice");
			return (Criteria) this;
		}

		public Criteria andMaxPriceNotEqualTo(Double value) {
			addCriterion("max_price <>", value, "maxPrice");
			return (Criteria) this;
		}

		public Criteria andMaxPriceGreaterThan(Double value) {
			addCriterion("max_price >", value, "maxPrice");
			return (Criteria) this;
		}

		public Criteria andMaxPriceGreaterThanOrEqualTo(Double value) {
			addCriterion("max_price >=", value, "maxPrice");
			return (Criteria) this;
		}

		public Criteria andMaxPriceLessThan(Double value) {
			addCriterion("max_price <", value, "maxPrice");
			return (Criteria) this;
		}

		public Criteria andMaxPriceLessThanOrEqualTo(Double value) {
			addCriterion("max_price <=", value, "maxPrice");
			return (Criteria) this;
		}

		public Criteria andMaxPriceIn(List<Double> values) {
			addCriterion("max_price in", values, "maxPrice");
			return (Criteria) this;
		}

		public Criteria andMaxPriceNotIn(List<Double> values) {
			addCriterion("max_price not in", values, "maxPrice");
			return (Criteria) this;
		}

		public Criteria andMaxPriceBetween(Double value1, Double value2) {
			addCriterion("max_price between", value1, value2, "maxPrice");
			return (Criteria) this;
		}

		public Criteria andMaxPriceNotBetween(Double value1, Double value2) {
			addCriterion("max_price not between", value1, value2, "maxPrice");
			return (Criteria) this;
		}

		public Criteria andMaxDateIsNull() {
			addCriterion("max_date is null");
			return (Criteria) this;
		}

		public Criteria andMaxDateIsNotNull() {
			addCriterion("max_date is not null");
			return (Criteria) this;
		}

		public Criteria andMaxDateEqualTo(Date value) {
			addCriterionForJDBCDate("max_date =", value, "maxDate");
			return (Criteria) this;
		}

		public Criteria andMaxDateNotEqualTo(Date value) {
			addCriterionForJDBCDate("max_date <>", value, "maxDate");
			return (Criteria) this;
		}

		public Criteria andMaxDateGreaterThan(Date value) {
			addCriterionForJDBCDate("max_date >", value, "maxDate");
			return (Criteria) this;
		}

		public Criteria andMaxDateGreaterThanOrEqualTo(Date value) {
			addCriterionForJDBCDate("max_date >=", value, "maxDate");
			return (Criteria) this;
		}

		public Criteria andMaxDateLessThan(Date value) {
			addCriterionForJDBCDate("max_date <", value, "maxDate");
			return (Criteria) this;
		}

		public Criteria andMaxDateLessThanOrEqualTo(Date value) {
			addCriterionForJDBCDate("max_date <=", value, "maxDate");
			return (Criteria) this;
		}

		public Criteria andMaxDateIn(List<Date> values) {
			addCriterionForJDBCDate("max_date in", values, "maxDate");
			return (Criteria) this;
		}

		public Criteria andMaxDateNotIn(List<Date> values) {
			addCriterionForJDBCDate("max_date not in", values, "maxDate");
			return (Criteria) this;
		}

		public Criteria andMaxDateBetween(Date value1, Date value2) {
			addCriterionForJDBCDate("max_date between", value1, value2, "maxDate");
			return (Criteria) this;
		}

		public Criteria andMaxDateNotBetween(Date value1, Date value2) {
			addCriterionForJDBCDate("max_date not between", value1, value2, "maxDate");
			return (Criteria) this;
		}

		public Criteria andMinPriceIsNull() {
			addCriterion("min_price is null");
			return (Criteria) this;
		}

		public Criteria andMinPriceIsNotNull() {
			addCriterion("min_price is not null");
			return (Criteria) this;
		}

		public Criteria andMinPriceEqualTo(Double value) {
			addCriterion("min_price =", value, "minPrice");
			return (Criteria) this;
		}

		public Criteria andMinPriceNotEqualTo(Double value) {
			addCriterion("min_price <>", value, "minPrice");
			return (Criteria) this;
		}

		public Criteria andMinPriceGreaterThan(Double value) {
			addCriterion("min_price >", value, "minPrice");
			return (Criteria) this;
		}

		public Criteria andMinPriceGreaterThanOrEqualTo(Double value) {
			addCriterion("min_price >=", value, "minPrice");
			return (Criteria) this;
		}

		public Criteria andMinPriceLessThan(Double value) {
			addCriterion("min_price <", value, "minPrice");
			return (Criteria) this;
		}

		public Criteria andMinPriceLessThanOrEqualTo(Double value) {
			addCriterion("min_price <=", value, "minPrice");
			return (Criteria) this;
		}

		public Criteria andMinPriceIn(List<Double> values) {
			addCriterion("min_price in", values, "minPrice");
			return (Criteria) this;
		}

		public Criteria andMinPriceNotIn(List<Double> values) {
			addCriterion("min_price not in", values, "minPrice");
			return (Criteria) this;
		}

		public Criteria andMinPriceBetween(Double value1, Double value2) {
			addCriterion("min_price between", value1, value2, "minPrice");
			return (Criteria) this;
		}

		public Criteria andMinPriceNotBetween(Double value1, Double value2) {
			addCriterion("min_price not between", value1, value2, "minPrice");
			return (Criteria) this;
		}

		public Criteria andMinDateIsNull() {
			addCriterion("min_date is null");
			return (Criteria) this;
		}

		public Criteria andMinDateIsNotNull() {
			addCriterion("min_date is not null");
			return (Criteria) this;
		}

		public Criteria andMinDateEqualTo(Date value) {
			addCriterionForJDBCDate("min_date =", value, "minDate");
			return (Criteria) this;
		}

		public Criteria andMinDateNotEqualTo(Date value) {
			addCriterionForJDBCDate("min_date <>", value, "minDate");
			return (Criteria) this;
		}

		public Criteria andMinDateGreaterThan(Date value) {
			addCriterionForJDBCDate("min_date >", value, "minDate");
			return (Criteria) this;
		}

		public Criteria andMinDateGreaterThanOrEqualTo(Date value) {
			addCriterionForJDBCDate("min_date >=", value, "minDate");
			return (Criteria) this;
		}

		public Criteria andMinDateLessThan(Date value) {
			addCriterionForJDBCDate("min_date <", value, "minDate");
			return (Criteria) this;
		}

		public Criteria andMinDateLessThanOrEqualTo(Date value) {
			addCriterionForJDBCDate("min_date <=", value, "minDate");
			return (Criteria) this;
		}

		public Criteria andMinDateIn(List<Date> values) {
			addCriterionForJDBCDate("min_date in", values, "minDate");
			return (Criteria) this;
		}

		public Criteria andMinDateNotIn(List<Date> values) {
			addCriterionForJDBCDate("min_date not in", values, "minDate");
			return (Criteria) this;
		}

		public Criteria andMinDateBetween(Date value1, Date value2) {
			addCriterionForJDBCDate("min_date between", value1, value2, "minDate");
			return (Criteria) this;
		}

		public Criteria andMinDateNotBetween(Date value1, Date value2) {
			addCriterionForJDBCDate("min_date not between", value1, value2, "minDate");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceIsNull() {
			addCriterion("current_price is null");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceIsNotNull() {
			addCriterion("current_price is not null");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceEqualTo(Double value) {
			addCriterion("current_price =", value, "currentPrice");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceNotEqualTo(Double value) {
			addCriterion("current_price <>", value, "currentPrice");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceGreaterThan(Double value) {
			addCriterion("current_price >", value, "currentPrice");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceGreaterThanOrEqualTo(Double value) {
			addCriterion("current_price >=", value, "currentPrice");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceLessThan(Double value) {
			addCriterion("current_price <", value, "currentPrice");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceLessThanOrEqualTo(Double value) {
			addCriterion("current_price <=", value, "currentPrice");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceIn(List<Double> values) {
			addCriterion("current_price in", values, "currentPrice");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceNotIn(List<Double> values) {
			addCriterion("current_price not in", values, "currentPrice");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceBetween(Double value1, Double value2) {
			addCriterion("current_price between", value1, value2, "currentPrice");
			return (Criteria) this;
		}

		public Criteria andCurrentPriceNotBetween(Double value1, Double value2) {
			addCriterion("current_price not between", value1, value2, "currentPrice");
			return (Criteria) this;
		}

		public Criteria andTotalValuesIsNull() {
			addCriterion("total_values is null");
			return (Criteria) this;
		}

		public Criteria andTotalValuesIsNotNull() {
			addCriterion("total_values is not null");
			return (Criteria) this;
		}

		public Criteria andTotalValuesEqualTo(Double value) {
			addCriterion("total_values =", value, "totalValues");
			return (Criteria) this;
		}

		public Criteria andTotalValuesNotEqualTo(Double value) {
			addCriterion("total_values <>", value, "totalValues");
			return (Criteria) this;
		}

		public Criteria andTotalValuesGreaterThan(Double value) {
			addCriterion("total_values >", value, "totalValues");
			return (Criteria) this;
		}

		public Criteria andTotalValuesGreaterThanOrEqualTo(Double value) {
			addCriterion("total_values >=", value, "totalValues");
			return (Criteria) this;
		}

		public Criteria andTotalValuesLessThan(Double value) {
			addCriterion("total_values <", value, "totalValues");
			return (Criteria) this;
		}

		public Criteria andTotalValuesLessThanOrEqualTo(Double value) {
			addCriterion("total_values <=", value, "totalValues");
			return (Criteria) this;
		}

		public Criteria andTotalValuesIn(List<Double> values) {
			addCriterion("total_values in", values, "totalValues");
			return (Criteria) this;
		}

		public Criteria andTotalValuesNotIn(List<Double> values) {
			addCriterion("total_values not in", values, "totalValues");
			return (Criteria) this;
		}

		public Criteria andTotalValuesBetween(Double value1, Double value2) {
			addCriterion("total_values between", value1, value2, "totalValues");
			return (Criteria) this;
		}

		public Criteria andTotalValuesNotBetween(Double value1, Double value2) {
			addCriterion("total_values not between", value1, value2, "totalValues");
			return (Criteria) this;
		}

		public Criteria andFlowValuesIsNull() {
			addCriterion("flow_values is null");
			return (Criteria) this;
		}

		public Criteria andFlowValuesIsNotNull() {
			addCriterion("flow_values is not null");
			return (Criteria) this;
		}

		public Criteria andFlowValuesEqualTo(Double value) {
			addCriterion("flow_values =", value, "flowValues");
			return (Criteria) this;
		}

		public Criteria andFlowValuesNotEqualTo(Double value) {
			addCriterion("flow_values <>", value, "flowValues");
			return (Criteria) this;
		}

		public Criteria andFlowValuesGreaterThan(Double value) {
			addCriterion("flow_values >", value, "flowValues");
			return (Criteria) this;
		}

		public Criteria andFlowValuesGreaterThanOrEqualTo(Double value) {
			addCriterion("flow_values >=", value, "flowValues");
			return (Criteria) this;
		}

		public Criteria andFlowValuesLessThan(Double value) {
			addCriterion("flow_values <", value, "flowValues");
			return (Criteria) this;
		}

		public Criteria andFlowValuesLessThanOrEqualTo(Double value) {
			addCriterion("flow_values <=", value, "flowValues");
			return (Criteria) this;
		}

		public Criteria andFlowValuesIn(List<Double> values) {
			addCriterion("flow_values in", values, "flowValues");
			return (Criteria) this;
		}

		public Criteria andFlowValuesNotIn(List<Double> values) {
			addCriterion("flow_values not in", values, "flowValues");
			return (Criteria) this;
		}

		public Criteria andFlowValuesBetween(Double value1, Double value2) {
			addCriterion("flow_values between", value1, value2, "flowValues");
			return (Criteria) this;
		}

		public Criteria andFlowValuesNotBetween(Double value1, Double value2) {
			addCriterion("flow_values not between", value1, value2, "flowValues");
			return (Criteria) this;
		}

		public Criteria andDateIsNull() {
			addCriterion("date is null");
			return (Criteria) this;
		}

		public Criteria andDateIsNotNull() {
			addCriterion("date is not null");
			return (Criteria) this;
		}

		public Criteria andDateEqualTo(Date value) {
			addCriterionForJDBCDate("date =", value, "date");
			return (Criteria) this;
		}

		public Criteria andDateNotEqualTo(Date value) {
			addCriterionForJDBCDate("date <>", value, "date");
			return (Criteria) this;
		}

		public Criteria andDateGreaterThan(Date value) {
			addCriterionForJDBCDate("date >", value, "date");
			return (Criteria) this;
		}

		public Criteria andDateGreaterThanOrEqualTo(Date value) {
			addCriterionForJDBCDate("date >=", value, "date");
			return (Criteria) this;
		}

		public Criteria andDateLessThan(Date value) {
			addCriterionForJDBCDate("date <", value, "date");
			return (Criteria) this;
		}

		public Criteria andDateLessThanOrEqualTo(Date value) {
			addCriterionForJDBCDate("date <=", value, "date");
			return (Criteria) this;
		}

		public Criteria andDateIn(List<Date> values) {
			addCriterionForJDBCDate("date in", values, "date");
			return (Criteria) this;
		}

		public Criteria andDateNotIn(List<Date> values) {
			addCriterionForJDBCDate("date not in", values, "date");
			return (Criteria) this;
		}

		public Criteria andDateBetween(Date value1, Date value2) {
			addCriterionForJDBCDate("date between", value1, value2, "date");
			return (Criteria) this;
		}

		public Criteria andDateNotBetween(Date value1, Date value2) {
			addCriterionForJDBCDate("date not between", value1, value2, "date");
			return (Criteria) this;
		}

		public Criteria andPriceRatioIsNull() {
			addCriterion("price_ratio is null");
			return (Criteria) this;
		}

		public Criteria andPriceRatioIsNotNull() {
			addCriterion("price_ratio is not null");
			return (Criteria) this;
		}

		public Criteria andPriceRatioEqualTo(Double value) {
			addCriterion("price_ratio =", value, "priceRatio");
			return (Criteria) this;
		}

		public Criteria andPriceRatioNotEqualTo(Double value) {
			addCriterion("price_ratio <>", value, "priceRatio");
			return (Criteria) this;
		}

		public Criteria andPriceRatioGreaterThan(Double value) {
			addCriterion("price_ratio >", value, "priceRatio");
			return (Criteria) this;
		}

		public Criteria andPriceRatioGreaterThanOrEqualTo(Double value) {
			addCriterion("price_ratio >=", value, "priceRatio");
			return (Criteria) this;
		}

		public Criteria andPriceRatioLessThan(Double value) {
			addCriterion("price_ratio <", value, "priceRatio");
			return (Criteria) this;
		}

		public Criteria andPriceRatioLessThanOrEqualTo(Double value) {
			addCriterion("price_ratio <=", value, "priceRatio");
			return (Criteria) this;
		}

		public Criteria andPriceRatioIn(List<Double> values) {
			addCriterion("price_ratio in", values, "priceRatio");
			return (Criteria) this;
		}

		public Criteria andPriceRatioNotIn(List<Double> values) {
			addCriterion("price_ratio not in", values, "priceRatio");
			return (Criteria) this;
		}

		public Criteria andPriceRatioBetween(Double value1, Double value2) {
			addCriterion("price_ratio between", value1, value2, "priceRatio");
			return (Criteria) this;
		}

		public Criteria andPriceRatioNotBetween(Double value1, Double value2) {
			addCriterion("price_ratio not between", value1, value2, "priceRatio");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioIsNull() {
			addCriterion("current_ratio is null");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioIsNotNull() {
			addCriterion("current_ratio is not null");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioEqualTo(Double value) {
			addCriterion("current_ratio =", value, "currentRatio");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioNotEqualTo(Double value) {
			addCriterion("current_ratio <>", value, "currentRatio");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioGreaterThan(Double value) {
			addCriterion("current_ratio >", value, "currentRatio");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioGreaterThanOrEqualTo(Double value) {
			addCriterion("current_ratio >=", value, "currentRatio");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioLessThan(Double value) {
			addCriterion("current_ratio <", value, "currentRatio");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioLessThanOrEqualTo(Double value) {
			addCriterion("current_ratio <=", value, "currentRatio");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioIn(List<Double> values) {
			addCriterion("current_ratio in", values, "currentRatio");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioNotIn(List<Double> values) {
			addCriterion("current_ratio not in", values, "currentRatio");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioBetween(Double value1, Double value2) {
			addCriterion("current_ratio between", value1, value2, "currentRatio");
			return (Criteria) this;
		}

		public Criteria andCurrentRatioNotBetween(Double value1, Double value2) {
			addCriterion("current_ratio not between", value1, value2, "currentRatio");
			return (Criteria) this;
		}

		public Criteria andConfidenceIsNull() {
			addCriterion("confidence is null");
			return (Criteria) this;
		}

		public Criteria andConfidenceIsNotNull() {
			addCriterion("confidence is not null");
			return (Criteria) this;
		}

		public Criteria andConfidenceEqualTo(Double value) {
			addCriterion("confidence =", value, "confidence");
			return (Criteria) this;
		}

		public Criteria andConfidenceNotEqualTo(Double value) {
			addCriterion("confidence <>", value, "confidence");
			return (Criteria) this;
		}

		public Criteria andConfidenceGreaterThan(Double value) {
			addCriterion("confidence >", value, "confidence");
			return (Criteria) this;
		}

		public Criteria andConfidenceGreaterThanOrEqualTo(Double value) {
			addCriterion("confidence >=", value, "confidence");
			return (Criteria) this;
		}

		public Criteria andConfidenceLessThan(Double value) {
			addCriterion("confidence <", value, "confidence");
			return (Criteria) this;
		}

		public Criteria andConfidenceLessThanOrEqualTo(Double value) {
			addCriterion("confidence <=", value, "confidence");
			return (Criteria) this;
		}

		public Criteria andConfidenceIn(List<Double> values) {
			addCriterion("confidence in", values, "confidence");
			return (Criteria) this;
		}

		public Criteria andConfidenceNotIn(List<Double> values) {
			addCriterion("confidence not in", values, "confidence");
			return (Criteria) this;
		}

		public Criteria andConfidenceBetween(Double value1, Double value2) {
			addCriterion("confidence between", value1, value2, "confidence");
			return (Criteria) this;
		}

		public Criteria andConfidenceNotBetween(Double value1, Double value2) {
			addCriterion("confidence not between", value1, value2, "confidence");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table stock
	 * @mbggenerated  Sat Oct 17 10:47:23 CST 2015
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table stock
     *
     * @mbggenerated do_not_delete_during_merge Fri Oct 16 21:00:40 CST 2015
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}