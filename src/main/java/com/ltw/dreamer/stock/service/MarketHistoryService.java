package com.ltw.dreamer.stock.service;

import java.util.List;

import com.ltw.dreamer.stock.model.MarketHistory;
import com.ltw.dreamer.stock.model.Stock;

public interface MarketHistoryService {
	public void updateMarketHistory(Stock stock);

	public List<MarketHistory> queryMarketHistory(String code);

	public void fixMarketHistoryData(Stock stock);
}
