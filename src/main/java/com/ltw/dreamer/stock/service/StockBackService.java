package com.ltw.dreamer.stock.service;

import com.ltw.dreamer.stock.model.Stock;

public interface StockBackService {
	public void updateStockBackupInfo(Stock stock);
}
