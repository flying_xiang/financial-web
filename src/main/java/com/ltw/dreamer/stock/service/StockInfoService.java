package com.ltw.dreamer.stock.service;

import java.util.List;

import com.ltw.dreamer.stock.model.Stock;
import com.ltw.dreamer.stock.model.StockInfo;

public interface StockInfoService {
	public void updateStockDailyInfo(Stock stock);

	public StockInfo queryStockInfo(Stock code);
	
	public List<StockInfo> queryStockInfo(String stockCode);
	
	public void cleanDuplicatedData(Stock stock);
	public void cleanClosedDayData(Stock stock);
}
