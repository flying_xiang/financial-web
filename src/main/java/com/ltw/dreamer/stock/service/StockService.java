package com.ltw.dreamer.stock.service;

import java.util.List;
import com.ltw.dreamer.stock.model.Stock;

public interface StockService {
	public List<Stock> selectAllStock();
	public void updateStockBasicInfo(Stock stock);
	public List<Stock> queryAllStock();
}
