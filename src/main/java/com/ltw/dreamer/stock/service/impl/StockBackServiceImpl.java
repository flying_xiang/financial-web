package com.ltw.dreamer.stock.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.ltw.dreamer.common.util.ModelUtil;
import com.ltw.dreamer.stock.mapper.StockBackMapper;
import com.ltw.dreamer.stock.model.Stock;
import com.ltw.dreamer.stock.model.StockBack;
import com.ltw.dreamer.stock.model.StockBackExample;
import com.ltw.dreamer.stock.model.StockBackExample.Criteria;
import com.ltw.dreamer.stock.service.StockBackService;

@Service
public class StockBackServiceImpl implements StockBackService {
	public static final Logger LOGGER = Logger.getLogger(StockBackServiceImpl.class);
	@Resource
	private StockBackMapper mapper = null;
	
	public void updateStockBackupInfo(Stock stock) {
		Date date = stock.getDate();
		if (date == null) return ;
		
		StockBackExample example = new StockBackExample();
		Criteria criteria = example.createCriteria();
		criteria.andCodeEqualTo(stock.getCode());
		criteria.andDateEqualTo(stock.getDate());
		List<StockBack> stockBackList = mapper.selectByExample(example);
		if (stockBackList != null) {
			for (StockBack stockBack : stockBackList) 
				mapper.deleteByPrimaryKey(stockBack.getId());
		}
		
		StockBack stockBack = new StockBack();
		ModelUtil.load(stock, stockBack);
		mapper.insert(stockBack);
	}
}
