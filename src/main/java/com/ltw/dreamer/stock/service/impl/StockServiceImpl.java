package com.ltw.dreamer.stock.service.impl;

import java.util.Calendar;
import java.util.List;
import javax.annotation.Resource;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import com.ltw.dreamer.common.util.NumbericUtil;
import com.ltw.dreamer.engine.pojo.StockIndexEnum;
import com.ltw.dreamer.engine.service.StockAnalyseService;
import com.ltw.dreamer.stock.mapper.StockMapper;
import com.ltw.dreamer.stock.model.Stock;
import com.ltw.dreamer.stock.model.StockExample;
import com.ltw.dreamer.stock.model.StockInfo;
import com.ltw.dreamer.stock.service.StockInfoService;
import com.ltw.dreamer.stock.service.StockService;

@Service
public class StockServiceImpl implements StockService {
	public static final Logger LOGGER = Logger.getLogger(StockServiceImpl.class);
	
	@Resource 
	private StockInfoService stockInfoService = null;
	
	@Resource
	private StockAnalyseService stockAnalyseService = null;
	
	@Resource
	private StockMapper mapper = null;
	
	public List<Stock> selectAllStock() {
		return mapper.selectByExample(new StockExample());
	}

	public void updateStockBasicInfo(Stock stock) {
		LOGGER.info("开始更新股票基本信息，股票代码：" + stock.getCode());
			
		// 清空最低价和最高价信息
		stock.setMaxPrice(null);
		stock.setMinPrice(null);
		stock.setPriceRatio(null);
		stock.setCurrentRatio(null);
		
		setStockInfo(stock);
		
		stock.setUpdatedDate(Calendar.getInstance().getTime());
		mapper.updateByPrimaryKey(stock);
		LOGGER.info("结束更新股票基本信息，股票代码：" + stock.getCode());
	}
	
	private void setStockInfo(Stock stock) {
		List<StockInfo> stockInfoList = stockInfoService.queryStockInfo(stock.getCode());
		if (stockInfoList != null && stockInfoList.size() > 0) {
			stock.setFlowValues(stockInfoList.get(0).getFlowValues());
			stock.setTotalValues(stockInfoList.get(0).getTotalValues());
			stock.setCurrentPrice(stockInfoList.get(0).getCurPrice());
			stock.setDate(stockInfoList.get(0).getDate());
		}
		
		stock.setConfidence(null);
		stock.setPriceRatio(null);
		stock.setCurrentRatio(null);
		stock.setMaxPrice(null);
		stock.setMinPrice(null);
		if (stock.getCode().equals("000811")) {
			System.out.println("START DEBUG");
		}
		if (!stockAnalyseService.shouldAnalyse(stockInfoList)) return ;
		stock.setConfidence(NumbericUtil.formatNumber(stockAnalyseService.calStockIndex(StockIndexEnum.DURATION_INDEX, stockInfoList)));
		stock.setTotalValues(NumbericUtil.formatNumber(stockAnalyseService.calStockIndex(StockIndexEnum.CHANGE_RATE_INDEX, stockInfoList)));
		stock.setPriceRatio(NumbericUtil.formatNumber(stockAnalyseService.calStockIndex(StockIndexEnum.UPPER_LINE_INDEX, stockInfoList)));
		stock.setCurrentRatio(NumbericUtil.formatNumber(stockAnalyseService.calStockIndex(StockIndexEnum.UPPER_LINE_RATE_INDEX, stockInfoList)));
		stock.setMinPrice(NumbericUtil.formatNumber(stockAnalyseService.calStockIndex(StockIndexEnum.DOWN_LINE_INDEX, stockInfoList)));
		stock.setMaxPrice(NumbericUtil.formatNumber(stockAnalyseService.calStockIndex(StockIndexEnum.DOWN_LINE_RATE_INDEX, stockInfoList)));		
	}
	
	public List<Stock> queryAllStock() {
		StockExample example = new StockExample();
		example.setOrderByClause("code");
		return mapper.selectByExample(example); 
	}
}
