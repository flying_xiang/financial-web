package com.ltw.dreamer.stock.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class StockInfoUtil {
	private static final StockInfoUtil instance = new StockInfoUtil();
	
	private static final Logger logger = Logger.getLogger(StockUtil.class);
	
	private Pattern stockPattern = Pattern.compile(".*\\\"(.*)\\\".*");
	private Matcher stockMatcher = null;
	private String value = null;
	
	private StockInfoUtil() {
		
	}
	
	public static synchronized boolean compile(String data) {
		instance.value = null;
		
		if (data == null) return false;
		instance.stockMatcher = instance.stockPattern.matcher(data.trim());
		
		if (instance.stockMatcher.matches()) {
			try {
				instance.value = instance.stockMatcher.group(1);
				return true;
			} catch (IllegalStateException ex) {
				logger.info("股票基础信息解析失败。" + ex.getLocalizedMessage());
				return false;
			}
		}
		return false;
	}
	
	public static String getValue() {
		return instance.value;
	}
}
