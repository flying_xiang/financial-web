package com.ltw.dreamer.stock.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class StockUtil {
	private static final StockUtil instance = new StockUtil();
	
	private static final Logger logger = Logger.getLogger(StockUtil.class);
	
	private Pattern stockPattern = Pattern.compile("(.*)(\\((.*)\\))");
	private Matcher stockMatcher = null;
	private String stockName = null;
	private String stockCode = null;
	private StockUtil() {
		
	}
	
	public static synchronized void compile(String data) {
		instance.stockName = null;
		instance.stockCode = null;
		
		if (data == null) return ;
		instance.stockMatcher = instance.stockPattern.matcher(data);
		
		if (instance.stockMatcher.matches()) {
			try {
				instance.stockName = instance.stockMatcher.group(1);
				instance.stockCode = instance.stockMatcher.group(3);
			} catch (IllegalStateException ex) {
				logger.info("股票信息匹配失败。" + ex.getLocalizedMessage());
			}
		}
	}
	
	public static String getStockCode() {
		return instance.stockCode;
	}
	
	public static String getStockName() {
		return instance.stockName;
	}
}
