package com.ltw.dreamer.common.schedule;

import javax.annotation.Resource;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.ltw.dreamer.common.schedule.MarketHistoryTask;

@ContextConfiguration(locations = { "classpath:config/applicationContext.xml" })
public class MarketHistoryTaskTest extends AbstractTestNGSpringContextTests {
	@Resource
	MarketHistoryTask task = null;

	@Test
	public void testUpdateMarketHistory() throws InterruptedException {
		// task.updateMarketHistory();
	}
	
	@Test
	public void testFixMarketHistoryData() throws InterruptedException {
		// task.fixMarketHistoryData();
	}
}
