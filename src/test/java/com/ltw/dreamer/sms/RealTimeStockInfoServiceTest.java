package com.ltw.dreamer.sms;

import java.util.List;

import javax.annotation.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;
import com.ltw.dreamer.sms.service.RealTimeStockInfoService;
import com.ltw.dreamer.stock.model.Stock;
import com.ltw.dreamer.stock.service.StockBackService;
import com.ltw.dreamer.stock.service.StockInfoService;
import com.ltw.dreamer.stock.service.StockService;

@ContextConfiguration(locations={"classpath:config/applicationContext.xml"}) 
public class RealTimeStockInfoServiceTest extends AbstractTestNGSpringContextTests {
	@Resource
	private RealTimeStockInfoService realTimeStockInfoService = null;
	
	@Resource
	private StockInfoService stockInfoService = null;
	
	@Resource
	private StockBackService stockBackService = null;
	
	@Resource
	private StockService stockService = null;
	
	@Test
	public void testNotifyRealTimeStockInfo() {
		List<Stock> stocks = stockService.selectAllStock();
		if (stocks == null) return ;

		for (Stock stock : stocks) {
			stockInfoService.updateStockDailyInfo(stock);
			stockService.updateStockBasicInfo(stock);
			stockBackService.updateStockBackupInfo(stock);
		}
		
		realTimeStockInfoService.notifyRealTimeStockInfo();
	}
}
