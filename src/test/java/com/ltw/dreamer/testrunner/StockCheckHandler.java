package com.ltw.dreamer.testrunner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.ltw.dreamer.stock.mapper.StockInfoMapper;
import com.ltw.dreamer.stock.model.Stock;
import com.ltw.dreamer.stock.model.StockInfo;
import com.ltw.dreamer.stock.model.StockInfoExample;
import com.ltw.dreamer.stock.model.StockInfoExample.Criteria;
import com.ltw.dreamer.stock.service.StockService;

/**
 * 算一下最近缩水最严重的股票，并顺便排个序输出看看
 * @author joyice
 *
 */
@ContextConfiguration(locations = { "classpath:config/applicationContext.xml" })
public class StockCheckHandler extends AbstractTestNGSpringContextTests {
	@Resource
	private StockService stockService = null;
	
	@Resource
	private StockInfoMapper mapper = null;
	
	@Test
	public void testCheckStock() {
		System.out.println("Do it!");
		
		List<Stock> stocks = pickUpAll300Stocks();
		
		List<DataStructure> wineInList = calculate(stocks);
		Collections.sort(wineInList);
		
		for (DataStructure data : wineInList) {
			System.out.println(data.getStock().getCode() + ", " + data.getStock().getName()+ ": " + data.getValue() + ", " + data.getCurPrice());
		}
	}

	private List<Stock> pickUpAll300Stocks() {
		List<Stock> result = new ArrayList<Stock>();
		
		List<Stock> stocks = stockService.queryAllStock();
		for (Stock stock : stocks) 
			if (stock.getCode().startsWith("300"))
				result.add(stock);
		
		return result;
	}
	
	private List<DataStructure> calculate(List<Stock> stocks) {
		List<DataStructure> result = new ArrayList<DataStructure>();
		
		for (Stock stock : stocks) {
			StockInfo stockInfoOf1228 = getStockInfo1228(stock);
			StockInfo stockInfoOf0203 = getStockInfo0203(stock);
			if (stockInfoOf1228 == null || stockInfoOf0203 == null) continue;
			
			DataStructure data = new DataStructure();
			data.setStock(stock);
			data.setCurPrice(stockInfoOf0203.getCurPrice());
			
			double oldPrice = stockInfoOf1228.getTodOpenPrice();
			double newPrice = stockInfoOf0203.getCurPrice();
			data.setValue((oldPrice - newPrice) / oldPrice);
			
			result.add(data);
		}
		
		return result;
	}


	private StockInfo getStockInfo1228(Stock stock) {
		StockInfoExample example = new StockInfoExample();
		Criteria criteria = example.createCriteria();
		criteria.andCodeEqualTo(stock.getCode());
		
		Calendar calender = Calendar.getInstance();
		calender.set(2015, 11, 28);
		
		criteria.andDateEqualTo(calender.getTime());
		List<StockInfo> queryResult = mapper.selectByExample(example);
		
		if (queryResult.size() == 0) return null;
		return queryResult.get(0);
	}

	private StockInfo getStockInfo0203(Stock stock) {
		StockInfoExample example = new StockInfoExample();
		Criteria criteria = example.createCriteria();
		criteria.andCodeEqualTo(stock.getCode());
		
		Calendar calender = Calendar.getInstance();
		calender.set(2016, 01, 03);
		
		criteria.andDateEqualTo(calender.getTime());
		List<StockInfo> queryResult = mapper.selectByExample(example);
		
		if (queryResult.size() == 0) return null;
		return queryResult.get(0);
	}
}

class DataStructure implements Comparable<DataStructure> {
	private Stock stock = null;
	private double value = 0.00;
	private double curPrice = 0.00;

	public int compareTo(DataStructure other) {
		return (int) ((this.value - other.value) * 1000) * -1;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public double getCurPrice() {
		return curPrice;
	}

	public void setCurPrice(double curPrice) {
		this.curPrice = curPrice;
	}
	
}
